@extends('layouts.app')

@section('content')
    <div class="row mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Busqueda Avanzada</h5>
                </div>
                <div class="card-body">
                    <div class="float-right" id="advanced-search-wrapper">
                        <button type="button" class="btn btn-primary" id="bttnShowAll" style="display:none;">Mostrar
                            todos</button>
                        <button type="button" class="btn btn-primary" id="bttn-advanced-search">
                            <i class="fas fa-search"></i> Búsqueda Avanzada
                        </button>
                        <span id="msjExport"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">

                <table class="table table-bordered table-sm" id="newsletter-table">
                    <thead>
                        <tr>
                            <th>CREADO</th>
                            <th>NOMBRE</th>
                            <th>APELLIDO</th>
                            <th>CORREO</th>
                            <th>TELEFONO</th>
                            <th>FECHA NAC.</th>
                            <th>GENERO</th>
                            <th>ESTADO</th>
                            <th>TODAS LAS CATEGORIAS</th>
                            <th>PLATAFORMAS</th>
                            <th>CON AGUJETA</th>
                            <th>BOTINES</th>
                            <th>SLIP ON</th>
                            <th>LISOS</th>
                            <th>SPRINTS</th>
                            <th>BLANCOS</th>
                            <th>COLABORACIONES</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@stop
@push('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript"
        src="https://cdn.datatables.net/v/bs5/jszip-2.5.0/dt-1.11.3/b-2.1.1/b-colvis-2.1.1/b-html5-2.1.1/b-print-2.1.1/datatables.min.js">
    </script>

    <script>
        $(function() {
            function getTableData(tablename, params) {
                $('#' + tablename).DataTable().clear();
                $('#' + tablename).DataTable().destroy();
                $('#' + tablename).DataTable({
                    dom: 'Bfrtip',
                    buttons: [{
                            extend: 'csvHtml5',
                            title: 'registros-nesletter'
                        },
                        {

                            extend: 'excelHtml5',
                            title: 'registros-nesletter'
                        }
                    ],
                    ajax: {
                        "method": "GET",
                        "data": {
                            "params": params
                        },
                        "url": "{!! route('newsletter.data') !!}"
                    },
                    columns: [{
                            data: 'created_at',
                            name: 'CREADO'
                        },
                        {
                            data: 'name',
                            name: 'NOMBRE'
                        },
                        {
                            data: 'lastname',
                            name: 'APERLLIDO'
                        },
                        {
                            data: 'email',
                            name: 'CORREO'
                        },
                        {
                            data: 'phone',
                            name: 'TELEFONO'
                        },
                        {
                            data: 'birthdate',
                            name: 'FECHA NAC.'
                        },
                        {
                            data: 'gender',
                            name: 'GENERO'
                        },
                        {
                            data: 'state',
                            name: 'ESTADO'
                        },
                        {
                            data: 'categories',
                            name: 'TODAS LAS CATEGORIAS'
                        },
                        {
                            data: 'categoria_1',
                            name: 'PLATAFORMAS'
                        },
                        {
                            data: 'categoria_2',
                            name: 'CON AGUJETA'
                        },
                        {
                            data: 'categoria_3',
                            name: 'BOTINES'
                        },
                        {
                            data: 'categoria_4',
                            name: 'SLIP ON'
                        },
                        {
                            data: 'categoria_5',
                            name: 'LISOS'
                        },
                        {
                            data: 'categoria_6',
                            name: 'PRINTS'
                        },
                        {
                            data: 'categoria_7',
                            name: 'BLANCOS'
                        },
                        {
                            data: 'categoria_8',
                            name: 'COLABORACIONES'
                        },
                    ],
                    order: [
                        [0, 'asc']
                    ]
                });

            }
            getTableData('newsletter-table', 'all');

            $('#bttn-advanced-search').click(function() {

                bootbox.confirm({
                    title: "Buscar por fecha",
                    message: '<form id="some-form">' +
                        '<div class="form-group">' +
                        '<label for="date">Date</label>' +
                        '<input id="date_before" class="date span2 form-control input-sm" size="16" placeholder="mm/dd/yy" type="date" required>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<label for="date">Date</label>' +
                        '<input id="date_after" class="date span2 form-control input-sm" size="16" placeholder="mm/dd/yy" type="date" required>' +
                        '</div>' +
                        '</form>',
                    callback: function(result) {
                        var date_before = $('#date_before').val();
                        var date_after = $('#date_after').val();
                        var params = JSON.stringify({
                            'date_before': date_before,
                            'date_after': date_after
                        });
                        console.log(params);
                        console.log(result)
                        if (date_after != '' && date_before != '') {
                            getTableData('newsletter-table', params);
                            bootbox.hideAll();

                            $('#bttnShowAll').show();

                            $('#bttnShowAll').click(function() {
                                $(this).hide();
                                getTableData('newsletter-table', 'all');
                            });
                        }
                    }
                });
            });
        });
    </script>
@endpush
