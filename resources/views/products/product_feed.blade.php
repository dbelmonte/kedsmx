@extends('layouts.app')

@section('content')
   <!-- Page Header -->
   <div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
    <h1 class="page-title fw-semibold fs-18 mb-0">Productos Google/Facebook</h1>
    <div class="ms-md-1 ms-0">
        <nav>
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                <li class="breadcrumb-item active" aria-current="page">Productos</li>
            </ol>
        </nav>
    </div>
</div>

  <!-- Start::row-1 -->
  <div class="row">
    <div class="col-xl-12">
        <div class="card custom-card">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        Productos Google/Facebook
                    </div>
                    <div>
                        <a href="{{ route('product.export_feed.csv') }}" class="btn btn-primary">Exportar CSV</a>
                        <a href="{{ route('product.export_feed.excel') }}" class="btn btn-primary">Exportar Excel</a>
                    </div>
                </div>
            <div class="card-body">
                <table class="table table-responsive mb-0" id="products-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>MODELO</th>
                            <th>URL PROD.</th>
                            <th>ESTADO</th>
                            <th>URL IMG</th>
                            <th>PRECIO</th>
                            <th>PRECIO OFERT.</th>
                            <th>FECHA PROM.</th>
                            <th>MARCA</th>
                            <th>MATERIAL</th>
                            <th>DISPONIBILIDAD</th>
                            <th>STOCK</th>
                            <th>GENERO</th>
                            <th>EDAD</th>
                            <th>CATEGORIA</th>
                            <th>CATEGORIA ID</th>
                            <th>APLAZO</th>
                            <th>TALLA</th>
                            <th>SIST. TALLA</th>
                            <th>UPC BARCODE</th>
                            <th>COLOR</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!--End::row-1 -->

@endsection
@include('assets.datatables')
@push('scripts')
<script>
    $(function() {
        $('#products-table').DataTable({
            scrollX: true,
            processing: true,
            serverSide: true,
            ajax: '{!! route('product.data') !!}',
            columns: [
                {
                    data: 'id',
                    name: 'id'
                },
                { 
                    data: 'nombre',
                    name: 'nombre'
                },
                { 
                    data: 'url_product',
                    name: 'url_product'
                },
                { 
                    data: 'estado',
                    name: 'estado'
                },
                { 
                    data: 'url_imagen',
                    name: 'url_imagen'
                },
                { 
                    data: 'precio',
                    name: 'precio'
                },
                { 
                    data: 'precio_oferta',
                    name: 'precio_oferta'
                },
                { 
                    data: 'fecha_promo',
                    name: 'fecha_promo'
                },
                { 
                    data: 'marca',
                    name: 'marca'
                },
                { 
                    data: 'material',
                    name: 'material'
                },
                { 
                    data: 'disponibilidad',
                    name: 'disponibilidad'
                },
                { 
                    data: 'stock',
                    name: 'stock'
                },
                { 
                    data: 'genero',
                    name: 'genero'
                },
                { 
                    data: 'edad',
                    name: 'edad'
                },
                { 
                    data: 'categoria',
                    name: 'categoria'
                },
                { 
                    data: 'categoria_2',
                    name: 'categoria_2'
                },
                { 
                    data: 'installment_amount',
                    name: 'installment_amount'
                },
                { 
                    data: 'talla',
                    name: 'talla'
                },
                { 
                    data: 'sistema_talla',
                    name: 'sistema_talla'
                },
                { 
                    data: 'upc_barcode',
                    name: 'upc_barcode'
                },
                { 
                    data: 'color',
                    name: 'color'
                }

            ],
            order: [
                [2, 'desc']
            ]
        });

      
    });
</script>
@endpush