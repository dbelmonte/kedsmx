<!-- app-header -->
<header class="app-header">

    <!-- Start::main-header-container -->
    <div class="main-header-container container-fluid">

        <!-- Start::header-content-left -->
        <div class="header-content-left">

            <!-- Start::header-element -->
            <div class="header-element">
                <div class="horizontal-logo">
                    <a href="{{ route('home') }}" class="header-logo">
                        <img src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}" class="desktop-logo" style="height: 80%">
                        <img src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}" class="toggle-logo" style="height: 80%">
                        <img src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}" class="desktop-dark" style="height: 80%">
                        <img src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}" class="toggle-dark" style="height: 80%">
                    </a>
                </div>
            </div>
            <!-- End::header-element -->

            <!-- Start::header-element -->
            <div class="header-element">
                <!-- Start::header-link -->
                <a aria-label="Hide Sidebar" class="sidemenu-toggle header-link animated-arrow hor-toggle horizontal-navtoggle" data-bs-toggle="sidebar" href="javascript:void(0);"><span></span></a>
                <!-- End::header-link -->
            </div>
            <!-- End::header-element -->

        </div>
        <!-- End::header-content-left -->

        <!-- Start::header-content-right -->
        <div class="header-content-right">

            <!-- Start::header-element -->
            <div class="header-element header-search">
                <!-- Start::header-link -->
                <form action="{{ route('search') }}">
                <div class="input-group mb-3 mt-2">
                    
                        <input type="text" class="form-control" name="num" id="num" placeholder="Buscar...">
                        <button type="submit" class="input-group-text" id="basic-addon1"><i class="ri-search-line"></i></button>
                  
                </div>
            </form>
                <!-- End::header-link -->
            </div>
            <!-- End::header-element -->

            <!-- Start::header-element -->
            <div class="header-element">
                <!-- Start::header-link|dropdown-toggle -->
                <a href="#" class="header-link dropdown-toggle" id="mainHeaderProfile" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-expanded="false">
                    <div class="d-flex align-items-center">
                        <div class="me-sm-2 me-0">
                            <img src="{{ asset('images/user.png') }}" alt="img" width="32" height="32" class="rounded-circle">
                        </div>
                        <div class="d-sm-block d-none">
                            <p class="fw-semibold mb-0 lh-1">{{ Auth::user()['name'] }}</p>
                            {{-- <span class="op-7 fw-normal d-block fs-11">Web Designer</span> --}}
                        </div>
                    </div>
                </a>
                <!-- End::header-link|dropdown-toggle -->
                <ul class="main-header-dropdown dropdown-menu pt-0 overflow-hidden header-profile-dropdown dropdown-menu-end" aria-labelledby="mainHeaderProfile">
                    {{-- <li><a class="dropdown-item d-flex" href="profile.html"><i class="ti ti-user-circle fs-18 me-2 op-7"></i>Profile</a></li>
                    <li><a class="dropdown-item d-flex" href="mail.html"><i class="ti ti-inbox fs-18 me-2 op-7"></i>Inbox <span class="badge bg-success-transparent ms-auto">25</span></a></li>
                    <li><a class="dropdown-item d-flex border-block-end" href="to-do-list.html"><i class="ti ti-clipboard-check fs-18 me-2 op-7"></i>Task Manager</a></li>
                    <li><a class="dropdown-item d-flex" href="mail-settings.html"><i class="ti ti-adjustments-horizontal fs-18 me-2 op-7"></i>Settings</a></li>
                    <li><a class="dropdown-item d-flex border-block-end" href="javascript:void(0);"><i class="ti ti-wallet fs-18 me-2 op-7"></i>Bal: $7,12,950</a></li>
                    <li><a class="dropdown-item d-flex" href="chat.html"><i class="ti ti-headset fs-18 me-2 op-7"></i>Support</a></li> --}}
                    <li><a class="dropdown-item d-flex"  href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="ti ti-logout fs-18 me-2 op-7"></i>Salir</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                        class="d-none">
                        @csrf
                    </form>
                    </li>
                </ul>
            </div>  
            <!-- End::header-element -->

        </div>
        <!-- End::header-content-right -->

    </div>
    <!-- End::main-header-container -->

</header>
<!-- /app-header -->
<!-- Start::app-sidebar -->
<aside class="app-sidebar sticky" id="sidebar">

    <!-- Start::main-sidebar-header -->
    <div class="main-sidebar-header">
        <a href="index.html" class="header-logo">
            <img src="asse ts/images/brand-logos/desktop-logo.png" alt="logo" class="desktop-logo">
            <img src="assets/images/brand-logos/toggle-logo.png" alt="logo" class="toggle-logo">
            <img src="assets/images/brand-logos/desktop-dark.png" alt="logo" class="desktop-dark">
            <img src="assets/images/brand-logos/toggle-dark.png" alt="logo" class="toggle-dark">
        </a>
    </div>
    <!-- End::main-sidebar-header -->

    <!-- Start::main-sidebar -->
    <div class="main-sidebar" id="sidebar-scroll">

        <!-- Start::nav -->
        <nav class="main-menu-container nav nav-pills flex-column sub-open">
            <div class="slide-left" id="slide-left">
                <svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24" viewBox="0 0 24 24"> <path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z"></path> </svg>
            </div>
            <ul class="main-menu">
                <li class="slide active">
                    <a href="{{ route('home') }}" class="side-menu__item active">
                        <i class='bx bx-home-alt-2 side-menu__icon'></i>
                        <span class="side-menu__label">Inicio</span>
                    </a>
                </li>
                <li class="slide has-sub open">
                    <a href="javascript:void(0);" class="side-menu__item">
                        <i class="bx bx-error side-menu__icon"></i>
                        <span class="side-menu__label">Sitios</span>
                        <i class="fe fe-chevron-right side-menu__angle"></i>
                    </a>
                    <ul class="slide-menu child1 active" style="position: relative; left: 0px; top: 0px; margin: 0px; box-sizing: border-box; transform: translate(395px, 48px); display: block;" data-popper-placement="bottom" data-popper-escaped="">
                        <li class="slide side-menu__label1">
                            <a href="javascript:void(0)">Sitios</a>
                        </li>
                        <li class="slide">
                            <a href="https://piagui.engranedigital.com/facturacion" class="side-menu__item">Massmascas</a>
                        </li>
                        <li class="slide">
                            <a href="https://ninewest.engranedigital.com/facturacion" class="side-menu__item">Ninewest</a>
                        </li>
                        <li class="slide">
                            <a href="https://sperry.engranedigital.com/facturacion" class="side-menu__item">Sperry</a>
                        </li>
                        <li class="slide">
                            <a href="https://keds.engranedigital.com/facturacion" class="side-menu__item">Keds</a>
                        </li>
                        <li class="slide">
                            <a href="https://havainas.engranedigital.com/facturacion" class="side-menu__item">Havaianas</a>
                        </li>
                        <li class="slide">
                            <a href="https://almaenpena.engranedigital.com/facturacion" class="side-menu__item">Alma En Pena</a>
                        </li>
                        <li class="slide">
                            <a href="https://cat.engranedigital.com/facturacion" class="side-menu__item">Caterpilar</a>
                        </li>
                    </ul>
                </li>
                <li class="slide has-sub open">
                    <a href="javascript:void(0);" class="side-menu__item">
                        <i class='bx bxl-product-hunt side-menu__icon'></i>
                        <span class="side-menu__label">Productos</span>
                        <i class="fe fe-chevron-right side-menu__angle"></i>
                    </a>
                    <ul class="slide-menu child1 active" style="position: relative; left: 0px; top: 0px; margin: 0px; box-sizing: border-box; transform: translate(395px, 48px); display: block;" data-popper-placement="bottom" data-popper-escaped="">
                        <li class="slide side-menu__label1">
                            <a href="javascript:void(0)">Productos</a>
                        </li>
                        <li class="slide">
                            <a href="#" class="side-menu__item">Chat Bot</a>
                        </li>
                        <li class="slide">
                            <a href="{{ route('product.feed') }}" class="side-menu__item">Feed Google/Facebook</a>
                        </li>
                        <li class="slide">
                            <a href="{{ route('category.index') }}" class="side-menu__item">Categorias Google</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="slide-right" id="slide-right"><svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24" viewBox="0 0 24 24"> <path d="M10.707 17.707 16.414 12l-5.707-5.707-1.414 1.414L13.586 12l-4.293 4.293z"></path> </svg></div>
        </nav>
        <!-- End::nav -->

    </div>
    <!-- End::main-sidebar -->

</aside>
<!-- End::app-sidebar -->