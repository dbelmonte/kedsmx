@extends('layouts.app')

@section('content')
@include('layouts.messages.messages')
 <!-- Page Header -->
 <div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
    <h1 class="page-title fw-semibold fs-18 mb-0">Orden Detalle</h1>
    <div class="ms-md-1 ms-0">
        <nav>
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="#">Ordenes</a></li>
                <li class="breadcrumb-item active" aria-current="page">Detalle NC</li>
            </ol>
        </nav>
    </div>
</div>
<!-- Page Header Close -->


  <!-- Start::row-1 -->
<div class="row">
    <div class="col-xl-3">
        <div class="row">
            <div class="col-xl-12">
                <div class="card custom-card">
                    <div class="card-header d-flex justify-content-between">
                        <div>
                            <span class="badge bg-primary-transparent">
                                Fecha: {{ Carbon\Carbon::parse($notaNc->created)->format('d-m-Y') }}
                            </span>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="p-3">
                            <div class="d-flex flex-wrap mb-1">
                                <a href="javascript:void(0);" class="pe-2">
                                    <span class="avatar border text-muted text-primary"><i class="bi bi-receipt"></i></span>
                                </a>
                                <div class="flex-fill">
                                    <div class="d-flex flex-wrap align-items-center justify-content-between mb-1">
                                        <h4><span class="fw-semibold">#{{ $notaNc->serie }}{{ $notaNc->folio }}</span></h4>
                                        <span class="text-success text-end">
                                            <h5>{!! get_bill_status($notaNc->bill_status) !!}</h5>
                                        </span>
                                    </div>
                                    <div class="d-flex flex-wrap align-items-center justify-content-between fs-12 mb-3">
                                        <span class="text-muted"></span>
                                        @if ($notaNc->bill_status == 'confirmed' && $notaNc->verification_code)
                                            <h5><span class="badge bg-primary">{{ $notaNc->verification_code }}</span></h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <button class="flex-fill btn btn-sm btn-danger me-2" id="gen_nc" data-id="{{ $order->id_orden }}" data-folio="{{ $notaNc->folio }}">
                                    Generar NC
                                </button>
                                <a href="{{ route('order.show', $order->id_orden) }}" class="flex-fill btn btn-sm btn-dark">Regresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-9">
        <div class="row">
            <div class="col-xl-12">
                <div class="card custom-card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="card-title">
                            Productos de la orden
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="products" class="table table-no-more table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-start">Line Item ID</th>
                                        <th class="text-start">Talla</th>
                                        <th class="text-start">Producto</th>
                                        <th class="text-start">Codigo Alfa</th>
                                        <th class="text-start">Referencia</th>
                                        <th class="text-start">Cantidad</th>
                                        <th class="text-start">Total</th>
                                        <th class="text-start"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $product)
                                    <tr>
                                        <td data-title="line_item">{{ $product->line_item_id }}</td>
                                        <td data-title="line_item">{{ $product->codigo_alfa }}-{{ $product->talla }}</td>
                                        <td data-title="Nombre">{{ $product->title }}</td>
                                        <td data-title="Codigo ALFA">{{ $product->codigo_alfa }}</td>
                                        <td data-title="Referencia">{{ $product->referencia }}</td>
                                        <td data-title="Cantidad">{{ $product->cantidad }}
                                            @if ($product->nota_credito > 0)
                                                <span class="badge bg-danger">Devueltos: {{ $product->nota_credito }} </span>
                                            @endif
                                        </td>
                                        <td data-title="Precio">{{ $product->importe }}</td>
                                        <td>

                                            <button class="mb-1 mt-1 me-1 btn btn-sm btn-danger nota_credito"
                                                onclick="notaCredito({{ $product->line_item_id }}, {{ $product->id }})" title="Estatus NOTA CREDITO" data-id="{{ $product->id }}" data-qty="{{ $product->cantidad }}"><i class="bi bi-plus-circle-fill"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="card-footer border-top-0">
                        <div class="btn-list float-end">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End::row-1 -->
@stop
@push('scripts')
    <script>
         function notaCredito(product_id, id) {
            var token =
                '{{ csrf_token() }}';

            Swal.fire({
                title: 'Crear devolución',
                text: "Crear una devolución de este articulo, ingrese la cantidad que desea devolver.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                showLoaderOnConfirm: true,
                input: 'number',
                inputAttributes: {
                    min: 1
                },
                inputValidator: (value) => {
                    return new Promise((resolve) => {
                        if (value < 1) {
                            resolve('La cantidad ingresada debe ser 1 o mayor.')
                        } else {
                            resolve()
                        }
                    })
                },
                preConfirm: (qty) => {
                    console.log(qty)
                    let data = {
                        id: id,
                        line_item: product_id,
                        _token: token,
                        cantidad: qty
                    };
                    return fetch('{{ route('product.delete') }}', {
                            headers: {
                                'Content-Type': 'application/json',
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            method: 'POST',
                            body: JSON.stringify(data),
                        })
                        .then(response => {
                            if (!response.ok) {
                                throw new Error(response.statusText)

                            }
                            return response.json()
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {
                    console.log(result)
                    Swal.fire({
                        icon: `${result.value.icon}`,
                        title: `${result.value.msg}`,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                    }).then((result) => {
                        // Reload the Page
                        location.reload();
                    });
                }
            })
        }
         $(document).ready(function() {

            var inputOptions = new Promise(function(resolve) {
                resolve({
                    'Si': 'Si',
                    'No': 'No'
                });
            });
            $("#gen_nc").click(function(e) {
                e.preventDefault();
                const orden = document.querySelector('#gen_nc');
                var token =
                    '{{ csrf_token() }}';

                Swal.fire({
                    title: 'Generar Nota de Credito',
                    text: "Seleccione si quiere agregar el costo de envio a la nota de credito.",
                    icon: 'info',
                    input: 'radio',
                    inputValue: 'No',
                    inputOptions: inputOptions,
                    showCancelButton: true,
                    confirmButtonText: 'Aceptar',
                    showLoaderOnConfirm: true,
                    inputValidator: function(option) {
                        return new Promise(function(resolve, reject) {
                            if (option != '' || option != null) {
                                resolve();
                            } else {
                                reject('Selecciona una opción');
                            }
                        });
                    },
                    preConfirm: (option) => {
                        let data = {
                            id: orden.dataset.id,
                            folio: orden.dataset.folio,
                            opcion: option,
                            _token: token
                        };
                        return fetch('{{ route('order.notacredito') }}', {
                                headers: {
                                    'Content-Type': 'application/json',
                                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                                },
                                method: 'POST',
                                body: JSON.stringify(data)
                            })
                            .then(response => {
                                if (!response.ok) {
                                    throw new Error(response.statusText)

                                }
                                return response.json()
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        console.log(result)
                        Swal.fire({
                            icon: `${result.value.icon}`,
                            title: `${result.value.msg}`,
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then((result) => {
                            // Reload the Page
                            location.reload();
                        });
                    }
                })
            });
            var inputOptionsNC = new Promise(function(resolve) {
                resolve({
                    'CANCELACION': 'CANCELACION',
                    'RESERVA': 'RESERVA',
                    'VENTA': 'VENTA',
                    'CONFIRMACION': 'CONFIRMACION'
                });
            });
         });
    </script>
@endpush
