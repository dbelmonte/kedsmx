@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-6  offset-md-3">
        <!-- Page Header -->
        <div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
            <h1 class="page-title fw-semibold fs-18 mb-0">Productos Google/Facebook</h1>
            <div class="ms-md-1 ms-0">
                <nav>
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Productos</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-6  offset-md-3">
        <div class="card custom-card">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        Editar Categorias
                    </div>
                </div>
            <div class="card-body">
                <form action="{{ route('category.update', $category->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row mb-3">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Codigo ID Google</label>
                        <div class="col-sm-3">
                            <input type="number" name="id_google" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $category->id_google }}">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Categorias <br><sub>Separadas por comas</sub></label>
                        <div class="col-sm-10">
                            <textarea name="categories" id="categories" cols="50" rows="10"  class="form-control form-control-sm" pattern="^[0-9a-zA-Z]+(,[0-9a-zA-Z]+){0,11}$">{!! $category->categories !!}</textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <a href="{{ route('category.index') }}" class="btn btn-default">Volver</a>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection