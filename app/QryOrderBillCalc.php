<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QryOrderBillCalc extends Model
{
    protected $table = 'qry_order_bill_calc';
    public $timestamps = false;
    protected $primaryKey = 'id_orden';
    public $incrementing = false;
    protected $fillable = [
        'monto_subtotal', 'monto_impuestos', 'monto_total', 'monto_descuento', 'monto_total_descuento', 'monto_total_pagar',
    ];
}
