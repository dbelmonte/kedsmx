<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $table = 'order_address';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_orden', 'tipo', 'nombre', 'apellido_1', 'apellido_2', 'calle', 'no_interior', 'no_exterior', 'colonia',
        'poblacion', 'provincia', 'pais', 'codigo_postal', 'telefono',
    ];
}
