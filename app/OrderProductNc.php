<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProductNc extends Model
{
    protected $table = 'order_products_nc';
    protected $primaryKey = 'id';
    public $timestamps = false;
    // protected $filable = [
    //     'folio', 'line_item_id', 'id_orden', 'codigo_alfa', 'sku', 'cantidad', 'devolucion',
    //     'envio', 'cancelacion', 'nota_credito', 'importe', 'product_status', 'referencia',
    //     'talla', 'title', 'total_discount', 'variant_title', 'facturado',
    // ];
    protected $guarded = [];  
}
