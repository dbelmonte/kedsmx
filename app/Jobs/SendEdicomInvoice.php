<?php

namespace App\Jobs;

use App\Http\Controllers\EdicomClass;
use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendEdicomInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $idShopify;
    public $folio;
    public $serie;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($idShopify, $folio, $serie)
    {
        $this->folio = $folio;
        $this->idShopify = $idShopify;
        $this->serie = $serie;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $serie = config('app.SERIE_FACTURA');
        $statusProducto = 'preparado';
        $EfectoComprobante = 'I';
        $TipoCFD =  false;
        $Misc32 = true;
        if ($this->serie == config('app.SERIE_NC')) {
            $serie = config('app.SERIE_NC');
            $statusProducto = 'nota_credito';
            $EfectoComprobante = 'E';
            $TipoCFD =  true;
            $Misc32 = false;
        }
        try {
            $order = Order::findOrFail($this->idShopify);
            if ($order->payment_method == 'Bank Deposit') {
                $billPaymentMethod = '03';
            } else {
                $billPaymentMethod = '04';
            }
            $edicomBill = new EdicomClass();
            $edicomBill->SerieComprobante = $serie;
            $edicomBill->idOrdenShopify = $order->id_orden;
            $edicomBill->ordenNum = $order->orden_num;
            $edicomBill->RFCReceptor = $order->cliente_rfc;
            $edicomBill->ordenEmailClient = $order->email_envio;
            $edicomBill->statusProducto = $statusProducto;
            $edicomBill->EfectoComprobante = $EfectoComprobante;
            if ($TipoCFD) {
                $edicomBill->TipoCFD = 'NC';
            }
            $edicomBill->FormaPago = $billPaymentMethod;
            if ($order->flete == 1) {
                $edicomBill->envioEstandarPrecioConIva = floatval($order->shipping_price);
            } else {
                $edicomBill->envioEstandarPrecioConIva = 0;
            }
            if ($Misc32 && $order->cliente_rfc != '') {
                $edicomBill->Misc32 = $order->uso_cfdi;
            }
            $edicomBill->createBill();

            Log::debug('Generacion de factura exitosa');
            // return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'Factura generada correctamente'], 200);
        } catch (\Throwable $th) {
            Log::debug($th);
            // return response()->json(['ok' => false, 'icon' => 'error', 'msg' => 'Error al general la factura'], 200);
        }
    }
}
