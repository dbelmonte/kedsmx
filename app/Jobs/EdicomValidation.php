<?php

namespace App\Jobs;

use App\EdicomBill;
use App\Services\EdicomService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Mtownsend\XmlToArray\XmlToArray;

class EdicomValidation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $reference;
    public $idShopify;
    public $folio;
    public $serie;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($reference, $idShopify, $folio, $serie)
    {
        $this->reference = $reference;
        $this->folio = $folio;
        $this->idShopify = $idShopify;
        $this->serie = $serie;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $Edicom = new EdicomService();
        $data = $Edicom->getDocumentB64($this->reference);
        Log::debug($data);
        if (isset($data['error'])) {
           SendEdicomInvoice::dispatch($this->idShopify, $this->folio, $this->serie);
        //     Log::debug('no existe la factura o nc, se envia a facturar');
           return null;
        }

        $dataXML = base64_decode($data['documentB64']);
        $array = XmlToArray::convert($dataXML);

        $edicomBill = EdicomBill::where('serie', '=', $this->serie)->where('folio', '=', $this->folio)->where('id_orden', '=', $this->idShopify)->first();

        $edicomBill->bill_status = 'confirmed';
        $edicomBill->verification_code = $this->folio;
        if (empty($edicomBill->uuid_factura)) {
          $edicomBill->uuid_factura = $array['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['UUID'];
        }
        if ($edicomBill->serie == config('app.SERIE_NC') and empty($edicomBill->uuid_factura_relacional)) {
            $invoiceFA = EdicomBill::where('id_orden', '=', $this->idShopify)->where('serie', '=', config('app.SERIE_FACTURA'))->first();
            $edicomBill->uuid_factura_relacional = $invoiceFA->uuid_factura;
        }
        $edicomBill->update();
        return null;
    }
}
