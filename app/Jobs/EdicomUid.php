<?php

namespace App\Jobs;

use App\EdicomBill;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class EdicomUid implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $idshopify;
    public $serie;
    public $folio;
    public $reference;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($idshopify, $serie, $folio, $reference)
    {
        $this->idshopify = $idshopify;
        $this->serie = $serie;
        $this->folio = $folio;
        $this->reference = $reference;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $uuid = getUuid($this->reference);
        $order = EdicomBill::where('id_orden', '=', $this->idshopify)->where('serie', '=', $this->serie)->where('folio', '=', $this->folio)->first();

        $order->uuid_factura = $uuid;
        $order->save();

    }
}
