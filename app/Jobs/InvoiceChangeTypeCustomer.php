<?php

namespace App\Jobs;

use App\Order;
use App\Services\OrderService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InvoiceChangeTypeCustomer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $idShopify;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($idShopify)
    {
        $this->idShopify = $idShopify;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = Order::where('id_orden', $this->idShopify)->first();
        $order->requiere_factura = "No";
        // $order->cliente_rfc = '';
        // $order->cfdi_code = '';
        // $order->uso_cfdi = '';
        $order->save();

        $facturar = new OrderService();
        $facturar->facturar($order);
    }
}
