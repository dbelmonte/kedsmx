<?php
namespace App\Services;

use App\Http\Controllers\EdicomClass;
use Illuminate\Support\Facades\Log;

class OrderService {

    public function CustomerData($data)
    {
        $response = [];

        if(isset($data->customer)){
            if ($data->customer->first_name == '' || $data->customer->last_name == '' || $data->customer->default_address->first_name == '' || $data->customer->default_address->last_name == '') {
              $response['nombre_completo'] = $data->shipping_address->first_name;
              $response['strApellidos'] = $data->shipping_address->last_name;
              $response['email_envio'] = $data->email;
            } else {
              $response['nombre_completo'] = $data->customer->first_name;
              $response['strApellidos'] = $data->customer->last_name;
              $response['email_envio'] = $data->customer->email;

            }
        }else{
            $response['nombre_completo'] = $data->shipping_address->first_name;
            $response['strApellidos'] = $data->shipping_address->last_name;
            $response['email_envio'] = $data->email;
        }

        return $response;

    }

    public function facturar($order)
    {
        try {
            if ($order->payment_method == 'Bank Deposit') {
                $billPaymentMethod = '03';
            } else {
                $billPaymentMethod = '04';
            }
            $edicomBill = new EdicomClass();
            $edicomBill->SerieComprobante = config('app.SERIE_FACTURA');
            $edicomBill->idOrdenShopify = $order->id_orden;
            $edicomBill->ordenNum = $order->orden_num;
            $edicomBill->RFCReceptor = $order->cliente_rfc;
            $edicomBill->ordenEmailClient = $order->email_envio;
            $edicomBill->statusProducto = 'preparado';
            $edicomBill->EfectoComprobante = 'I';
            $edicomBill->FormaPago = $billPaymentMethod;
            $edicomBill->envioEstandarPrecioConIva = floatval($order->shipping_price);
            $edicomBill->createBill();

            Log::debug('Generacion de factura exitosa');
            // return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'Factura generada correctamente'], 200);
        } catch (\Throwable $th) {
            Log::debug($th);
            // return response()->json(['ok' => false, 'icon' => 'error', 'msg' => 'Error al general la factura'], 200);
        }
    }

}
