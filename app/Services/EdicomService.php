<?php
namespace App\Services;

use App\EdicomBill;
use Illuminate\Support\Facades\Log;
use SoapClient;

class EdicomService
{
    public function getDocumentB64($folio)
    {
        $xmlreq = '<?xml version="1.0" encoding="UTF-8"?>';
        $xmlreq .= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ediw="http://ediwinIntegration.service.ediwinws.edicom.com">';
        $xmlreq .= '<soapenv:Header/>';
        $xmlreq .= '<soapenv:Body>';
        $xmlreq .= '<ediw:getUnzipDocument>';
        $xmlreq .= '<ediw:user>'.config('app.ediwin_usr').'</ediw:user>';
        $xmlreq .= '<ediw:password>'.config('app.ediwin_passwd').'</ediw:password>';
        $xmlreq .= '<ediw:domain>'.config('app.ediwin_domain').'</ediw:domain>';
        $xmlreq .= '<ediw:exportType>2</ediw:exportType>';
        $xmlreq .= '<ediw:idVolume>0</ediw:idVolume>';
        $xmlreq .= '<ediw:parameters>';
        $xmlreq .= '<ediw:name>REFERENCIA</ediw:name>';
        $xmlreq .= '<ediw:operator>=</ediw:operator>';
        $xmlreq .= '<ediw:value>'.$folio.'</ediw:value>';
        $xmlreq .= '</ediw:parameters>';
        $xmlreq .= '</ediw:getUnzipDocument>';
        $xmlreq .= '</soapenv:Body>';
        $xmlreq .= '</soapenv:Envelope>';

        $location_URL = "https://web.sedeb2b.com/EdiwinWS/services/EdiwinIntegration?wsdl";
        $client = new SoapClient(null, array(
            'location' => $location_URL,
            'uri' => $location_URL,
            'trace' => 1,
        ));

        try {
            $search_result = $client->__doRequest($xmlreq, $location_URL, $location_URL, 1);
            /* Convertir el mensaje a texto decodificando el mensaje */
            $dataXMLDecode = html_entity_decode($search_result);
            /* -------Parse de respuesta hermes---------- */
            $documentReturn = get_string_between($dataXMLDecode, '<getUnzipDocumentReturn>', '</getUnzipDocumentReturn>');
            $errorcode = get_string_between($dataXMLDecode, '<ns1:cod>', '</ns1:cod>');
            $errormeesaje = get_string_between($dataXMLDecode, '<ns1:text>', '</ns1:text>');

            if ($errorcode) {
                $response['code'] = $errorcode;
                $response['error'] = true;
                $response['message'] = $errormeesaje;
            }

            $response['code'] = 200;
            $response['documentB64'] = $documentReturn;

            return $response;

        } catch (\SoapFault $exception) {
            //var_dump(get_class($exception));
            // var_dump($exception);
                $response['code'] = 400;
                $response['error'] = true;
                $response['message'] = $exception->getMessage();
                return $response;
            // return response()->json(['code'=>400, 'error'=>true, 'message'=>$exception->getMessage()]);

        }
    }
}
