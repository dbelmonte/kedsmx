<?php

namespace App\Exports;

use App\ProductFeed;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProductFeedExport implements FromView
{
    
    public function view(): View
    {
        return view('exports.product_feed', [
            'products' => ProductFeed::where('stock', '>=', config('app.size_limit_stock'))->where('talla', '!=', '')->where('active', 0)->get()
        ]);
    }
}