<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    const CREATED_AT = 'created_at';
    protected $table = 'register';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = [
        'name', 'lastname', 'email', 'phone', 'birthdate', 'gender', 'state', 'categories', 'products', 'brands', 'send_email',
    ];
}
