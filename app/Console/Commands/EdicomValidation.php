<?php

namespace App\Console\Commands;

use App\EdicomBill;
use App\Jobs\EdicomValidation as JobsEdicomValidation;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class EdicomValidation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'edicom:validation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $id_shopify = 6166125281341;
        $date = Carbon::now()->subDays(1);
        // $date = '2024-05-29';
        $invoices = EdicomBill::where('bill_status', '!=', 'confirmed')->where('created', '>', $date)->get();
        // $invoices = EdicomBill::where('serie', '=', 'AML')->where('folio', '=', 2219491)->where('id_orden', '=', 6166837362749)->get();

        // dd($invoices);
        foreach($invoices as $invoice)
        {
            // JobsEdicomValidation::dispatch($invoice->folioInvoice());
            JobsEdicomValidation::dispatch($invoice->folioInvoice(), $invoice->id_orden, $invoice->folio, $invoice->serie);
        }


    }
}
