<?php

namespace App\Console\Commands;

use App\EdicomBill;
use App\Jobs\InvoiceChangeTypeCustomer;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InvoiceTypeValidation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:change-type-customer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dateSubDay = Carbon::now()->subDays(1);
        $dateNow = Carbon::now();
        // $date = '2024-05-29';
        $invoices = EdicomBill::where('bill_status', '=', 'error')->where('rfc', '!=', '')->where('created', '>=', $dateSubDay)->where('created', '<=', $dateNow)->get();
        // $invoices = EdicomBill::where('serie', '=', 'AML')->where('folio', '=', 2219491)->where('id_orden', '=', 6166837362749)->get();

        // dd($invoices);
        foreach($invoices as $invoice)
        {
            $dateInovice = Carbon::parse($invoice->created);
            $diff = $dateInovice->diffInHours($dateNow);
            if ($diff >= 24) {
                InvoiceChangeTypeCustomer::dispatch($invoice->id_orden);
            }
        }
    }
}
