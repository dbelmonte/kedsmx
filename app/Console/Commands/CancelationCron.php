<?php

namespace App\Console\Commands;

use App\Order;
use App\OrderAddress;
use App\Product;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use SoapClient;
use stdClass;

class CancelationCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cancelation:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancelacion de Ordenes por Deposito';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        $now = Carbon::now();

        $oneDay = 0;
        // $orders = Order::where('tipo_orden', 'R')->where('payment_method', 'Bank Deposit')->where('payment_status', 'pending')->get();
        $orders = Order::where('tipo_orden', 'R')->where('payment_method', 'Bank Deposit')->where('payment_status', '<>', 'paid')->get();
        foreach ($orders as $order) {
            $now = Carbon::now();
            // $crea = new Carbon($order->fecha_pedido);
            // $is_day = Carbon::parse($crea)->format('l');
            // if ($is_day == 'Friday') {
            //     $now->addDays(3);
            //     Log::debug('aca viernes');
            // }elseif ($is_day == 'Saturday') {
            //     $now->addDays(4);
            //     Log::debug('aca sabado');
            // }elseif ($is_day == 'Sunday') {
            //     $now->addDays(4);
            //     Log::debug('aca domingo');
            // }else{
            //     $now;
            // }
            // $hor = $crea->diffInHours($now);
            // // echo $order->orden_num . ' horas '. $hor .' <br>';
            // if(($hor > 72 && $is_day == 'Monday') || ($hor > 72 && $is_day == 'Tuesday') || ($hor > 72 && $is_day == 'Wednesday') || ($is_day == 'Thursday' && $hor >= 120) || ($is_day == 'Friday' && $hor >= 120) || ($is_day == 'Saturday' && $hor >= 140) || ($is_day == 'Sunday' && $hor >= 160)){
            
            $from = Carbon::parse($order->created);
            $to = Carbon::parse($now);
            $diff = $to->diffInWeekdays($from);
            // $holidays=array('2023-03-20');
            $holidays=$this->getHolidays();
            $diferencia = $this->getWorkingDays($from,$now,$holidays);
            // Log::debug('diferencias ' . floor($diferencia) );
            
            if ($diferencia > 4) {
                // if($diffDays >=3){
                    $hermes = $this->sendHermesData($order->id_orden, $order->orden_num, $order->fecha_pedido, $order->shipping_method, $order->cac_store_id, $order->email_envio, $order->importe_total);
                        $updateOrder = $this->updateOrder($order->id_orden);
                        $shopify = $this->sendToShopify($order->id_orden);
                // } 
                Log::debug('lista para cancelar ' . $order->orden_num);
            }
            Log::debug('la orden '.$order->orden_num.' le faltan: '.$diff);
        }
        // Log::debug("Cron is working fine!");

    }

    function getWorkingDays($startDate,$endDate,$holidays) {
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);
    
    
        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        // $days = ($endDate - $startDate) / 86400 + 1;
        $days = ($endDate - $startDate) / 86400;
    
        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);
    
        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);
    
        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        }
        else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)
    
            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;
    
                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            }
            else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }
    
        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
    //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
       $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0 )
        {
          $workingDays += $no_remaining_days;
        }
    
        //We subtract the holidays
        foreach($holidays as $holiday){
            $time_stamp=strtotime($holiday);
            Log::debug($time_stamp);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                $workingDays--;
                Log::debug($workingDays);
        }
    
        return $workingDays;
    }

    function getHolidays()
    {
        $client = new \GuzzleHttp\Client();
        $url = 'http://ninewest.engranedigital.com/facturacion/get-holidays';
        $parameters = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ];
        if(!empty($param)){ $parameters['json'] = $param;}
        $response = $client->request('GET', $url,$parameters);

        return json_decode($response->getBody());
    }

    public function updateOrder($id_order)
    {
        $update = DB::table('orders')
            ->where('id_orden', '=', $id_order)
            ->update([
                'tipo_orden' => 'X',
            ]);
        if ($update) {
            Log::debug("Actualizacion correcta!");
        } else {
            Log::debug("Error al actualizar!");
        }

    }

    public function sendToShopify($orderId)
    {
        $url = "https://" . config('app.api_key') . ":" . config('app.api_pass') . "@" . config('app.shop_name') . ".myshopify.com/admin/api/" . config('app.api_ver') . "/orders/" . $orderId . "/cancel.json";

        $curl = curl_init();

        $data = new stdClass();
        $data->reason = 'declined';

        $jsonData = json_encode($data);

        //Set the URL that you want to GET by using the CURLOPT_URL option.
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
        ));
        //curl_setopt($curl, CURLOPT_HEADER, true);

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonData);

        //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //additional options
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        //Execute the request.
        $result = curl_exec($curl);
        Log::debug($result);
    }

    public function sendHermesData($id_orden, $order_num, $fecha_pedido, $tipo_envio, $id_tienda, $email_envio, $importe_total)
    {

        // $direccion = $link->query("SELECT * FROM order_address where id_orden = $id_orden and tipo = 'shipping' ");
        // $sql = "SELECT * FROM order_address where id_orden = '$id_orden' and tipo = 'shipping' ";
        $address = OrderAddress::where('id_orden', $id_orden)->where('tipo', 'shipping')->get();
        $prod = '';

        foreach ($address as $direccion) {
            $products = Product::where('id_orden', $id_orden)->get();
            foreach ($products as $producto) {
                $prod .= '<PRODUCTO>';
                $prod .= '<CODIGO_ALFA>' . $producto->codigo_alfa . '</CODIGO_ALFA>';
                $prod .= '<REFERENCIA>' . $producto->codigo_alfa . '</REFERENCIA>';
                $prod .= '<NUMERO_TIENDA_EXTERNA>1303</NUMERO_TIENDA_EXTERNA>';
                $prod .= '<TALLA>' . $producto->talla . '</TALLA>';
                $prod .= '<IMPORTE>' . $producto->importe . '</IMPORTE>';
                $prod .= '<CANTIDAD>' . $producto->cantidad . '</CANTIDAD>';
                $prod .= '</PRODUCTO>';
            }
            $xmlreq = '<?xml version="1.0" encoding="UTF-8"?>';
            $xmlreq .= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://server.hermes.ecommerce.hermes.com">';
            $xmlreq .= '<soapenv:Header/>';
            $xmlreq .= '<soapenv:Body>';
            $xmlreq .= '<ser:insertOrder>';
            $xmlreq .= '<ser:login>shopify</ser:login>';
            $xmlreq .= '<ser:password>Shopify123$</ser:password>';
            $xmlreq .= '<ser:idSite>263</ser:idSite>';
            $xmlreq .= '<ser:in0>';
            $xmlreq .= '<![CDATA[<SOLICITUDES>';
            $xmlreq .= '<SOLICITUD>';
            $xmlreq .= '<TIPO>CANCELACION</TIPO>';
            $xmlreq .= '<NUMERO_PEDIDO_ORIGINAL>' . $order_num . '</NUMERO_PEDIDO_ORIGINAL>';
            $xmlreq .= '<NUMERO_PEDIDO_ALTERNATIVO>' . $order_num . '</NUMERO_PEDIDO_ALTERNATIVO>';
            $xmlreq .= '<NUMERO_TIENDA_EXTERNA>1303</NUMERO_TIENDA_EXTERNA>';
            $xmlreq .= '<FECHA_PEDIDO>' . $fecha_pedido . '</FECHA_PEDIDO>';
            $xmlreq .= '<TIPO_ENVIO>' . $tipo_envio . '</TIPO_ENVIO>';
            $xmlreq .= '<ID_TIENDA>' . $id_tienda . '</ID_TIENDA>';
            $xmlreq .= '<NOMBRE_ENVIO>' . $direccion->nombre . '</NOMBRE_ENVIO>';
            $xmlreq .= '<APELLIDO_1_ENVIO>' . $direccion->apellido_1 . '</APELLIDO_1_ENVIO>';
            $xmlreq .= '<APELLIDO_2_ENVIO>' . $direccion->apellido_2 . '</APELLIDO_2_ENVIO>';
            $xmlreq .= '<DOMICILIO_ENVIO>' . $direccion->calle . '</DOMICILIO_ENVIO>';
            $xmlreq .= '<NUMERO_EXTERIOR_ENVIO>' . $direccion->no_exterior . '</NUMERO_EXTERIOR_ENVIO>';
            $xmlreq .= '<NUMERO_INTERIOR_ENVIO>' . $direccion->no_interior . '</NUMERO_INTERIOR_ENVIO>';
            $xmlreq .= '<COLONIA_ENVIO>' . $direccion->colonia . '</COLONIA_ENVIO>';
            $xmlreq .= '<POBLACION_ENVIO>' . $direccion->poblacion . '</POBLACION_ENVIO>';
            $xmlreq .= '<PROVINCIA_ENVIO>' . $direccion->provincia . '</PROVINCIA_ENVIO>';
            $xmlreq .= '<CODIGO_POSTAL_ENVIO>' . $direccion->codigo_postal . '</CODIGO_POSTAL_ENVIO>';
            $xmlreq .= '<TELEFONO_ENVIO>' . $direccion->telefono . '</TELEFONO_ENVIO>';
            $xmlreq .= '<TELEFONO_MOVIL_ENVIO>' . $direccion->telefono . '</TELEFONO_MOVIL_ENVIO>';
            $xmlreq .= '<EMAIL_ENVIO>' . $email_envio . '</EMAIL_ENVIO>';
            $xmlreq .= '<OBSERVACIONES>N/A</OBSERVACIONES>';
            $xmlreq .= '<IMPORTE_TOTAL>' . $importe_total . '</IMPORTE_TOTAL>';
            $xmlreq .= '<FORMA_PAGO>21</FORMA_PAGO>';
            $xmlreq .= '<PRODUCTOS>';
            $xmlreq .= $prod;
            $xmlreq .= '</PRODUCTOS>';
            $xmlreq .= '<DESCUENTOS></DESCUENTOS>';
            $xmlreq .= '</SOLICITUD>';
            $xmlreq .= '</SOLICITUDES>]]>';
            $xmlreq .= '</ser:in0>';
            $xmlreq .= '</ser:insertOrder>';
            $xmlreq .= '</soapenv:Body>';
            $xmlreq .= '</soapenv:Envelope>';

        }
        //$location_URL="http://mypiagui.com:888/hermesService/services/HermesImpl?wsdl";
        $location_URL = "http://piaguimexico.com/hermesService/services/HermesImpl?wsdl";
        $client = new SoapClient(null, array(
            'location' => $location_URL,
            'uri' => $location_URL,
            'trace' => 1,
        ));

        try {
            $search_result = $client->__doRequest($xmlreq, $location_URL, $location_URL, 1);

            $dataXMLDecode = html_entity_decode($search_result);

            $pedidoOriginal = $this->get_string_between($dataXMLDecode, '<NUMERO_PEDIDO_ORIGINAL>', '</NUMERO_PEDIDO_ORIGINAL>');
            $respuestaHermes = $this->get_string_between($dataXMLDecode, '<RESPUESTA>', '</RESPUESTA>');
            $errorHermes = $this->get_string_between($dataXMLDecode, '<ERROR>', '</ERROR>');
            $pedidoHermes = $this->get_string_between($dataXMLDecode, '<PEDIDO_HERMES>', '</PEDIDO_HERMES>');
            $msjToHermes = $this->get_string_between($xmlreq, '<![CDATA[', ']]>');

            $response_xml = $client->__getLastResponse();
            Log::debug('Mensaje Hermes: ' . $msjToHermes);
            Log::debug('Respuesta Hermes: ' . $dataXMLDecode);
            if ($respuestaHermes == 'ERROR') {
                Log::debug('Error Hermes: ' . $dataXMLDecode);
            }

            $apiResponse['code'] = 200;
            $apiResponse['message'] = array(
                "response" => $respuestaHermes,
                "pedido_original" => $pedidoOriginal,
                "error_hermes" => $errorHermes,
                "pedido_hermes" => $pedidoHermes,
            );

        } catch (\SoapFault $exception) {
            $apiResponse['code'] = 200;
            $apiResponse['message'] = array(
                "response" => $errorHermes,
                "pedido_original" => $pedidoOriginal,
                "error_hermes" => $errorHermes,
                "pedido_hermes" => $pedidoHermes,
            );
            Log::debug('Error Hermes: ' . $dataXMLDecode);
        }
        return $apiResponse;
    }

    public function getProductos($id_orden)
    {
        $items = '';

        $productos = Product::where('id_orden', $id_orden)->get();

        foreach ($productos as $producto) {
            $items .= '<PRODUCTO>';
            $items .= '<CODIGO_ALFA>' . $producto->codigo_alfa . '</CODIGO_ALFA>';
            $items .= '<REFERENCIA>' . $producto->sap_code . '</REFERENCIA>';
            $items .= '<NUMERO_TIENDA_EXTERNA>1303</NUMERO_TIENDA_EXTERNA>';
            $items .= '<TALLA>' . $producto->talla . '</TALLA>';
            $items .= '<IMPORTE>' . $producto->importe . '</IMPORTE>';
            $items .= '<CANTIDAD>' . $producto->cantidad . '</CANTIDAD>';
            $items .= '</PRODUCTO>';
        }

        return $items;

    }

    public function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) {
            return '';
        }

        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}
