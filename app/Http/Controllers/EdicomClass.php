<?php

namespace App\Http\Controllers;

use SoapClient;
use ZipArchive;
use Carbon\Carbon;
use App\EdicomBill;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\EdicomBillLine;
use Luecano\NumeroALetras\NumeroALetras;
use App\Http\Controllers\OrderDaoController;
use App\Jobs\EdicomUid;

class EdicomClass extends Controller
{
    // Header privates
    public $Folio = ''; // Consecutivo de la factura
    private $NombreEmisor = 'LIFESTYLE AND HERITAGE BRANDS OF MEXICO';
    private $RFCEmisor = "LHB1508052E7";
    private $DomEmisorCalle = "Calle Valparaiso";
    private $DomEmisorNoExterior = "2319";
    private $DomEmisorNoInterior = "piso 3";
    private $DomEmisorColonia = "Colonia Providencia 1Ra Sección";
    private $DomEmisorLocalidad;
    private $DomEmisorReferencia;
    private $DomEmisorMunicipio = "Guadalajara";
    private $DomEmisorEstado = "Jalisco";
    private $DomEmisorPais = "México";
    private $DomEmisorCodigoPostal = 44690;
    private $TelEmisor;
    private $DomSucursalCalle;
    private $DomSucursalNoExterior;
    private $DomSucursalNoInterior;
    private $DomSucursalColonia;
    private $DomSucursalLocalidad;
    private $DomSucursalReferencia;
    private $DomSucursalMunicipio;
    private $DomSucursalEstado;
    private $DomSucursalPais;
    private $DomSucursalCodigoPostal;
    private $TelSucursal;
    private $Version = 4.0;
    public $SerieComprobante = 'ABK'; // ABK=Venta, BBK=Devolucion
    private $NumeroAprobacion;
    public $FormaPago = '99'; // 04=TDC Paypal, 03=Transferencia/deposito, 28=Debito, 99=por definir
    private $Fecha = ''; // Fecha de facturacion
    private $Hora = ''; // Hora de facturacion
    private $DomLugarExpideCalle;
    private $DomLugarExpideNoExterior;
    private $DomLugarExpideNoInterior;
    private $DomLugarExpideColonia;
    private $DomLugarExpideLocalidad;
    private $DomLugarExpideReferencia;
    private $DomLugarExpideMunicipio = "Guadalajara";
    private $DomLugarExpideEstado = "Jalisco";
    private $DomLugarExpidePais = "México";
    private $DomLugarExpideCodigoPostal = 44690;
    private $NombreReceptor = "Venta al público en general E-COMMERCE"; // Nombre del cliente
    public $RFCReceptor = "XAXX010101000"; // RFC del cliente
    private $DomReceptorCalle = "Calle Valparaiso";
    private $DomReceptorNoExterior = 2319;
    private $DomReceptorNoInterior = "piso 3";
    private $DomReceptorColonia = "Colonia Providencia 1Ra Sección";
    private $DomReceptorLocalidad;
    private $DomReceptorReferencia;
    private $DomReceptorMunicipio = "Guadalajara";
    private $DomReceptorEstado = "Jalisco";
    private $DomReceptorPais = "México";
    private $DomReceptorCodigoPostal = 44690;
    private $MontoSubTotal = 550.86; // Importe del Pedido Sin IVA
    private $MontoImpuestos = 0; // IVA
    private $MontoTotal = 0; // Subtotal + IVA
    private $Estado = 1;
    public $TipoCFD = "FA"; // FA=Factura, NC=Nota de credito
    private $Notas;
    private $Notas02;
    private $Notas03;
    private $TradingPartnerProv;
    private $CalifTradingPartnerProv;
    private $GLNProveedor;
    private $NumeroFactura;
    private $NumeroOrdenCompra;
    private $FechaOrdenCompra;
    private $NumeroProveedor;
    private $GLNTienda;
    private $NumeroTienda = '1303';
    private $NombreTienda = 'E-COMMERCE';
    private $DomTiendaCalle = "Calle Valparaiso";
    private $DomTiendaNoExterior = 2319;
    private $DomTiendaNoInterior = "piso 3";
    private $DomTiendaColonia = "Colonia Providencia 1Ra Sección";
    private $DomTiendaLocalidad;
    private $DomTiendaReferencia;
    private $DomTiendaMunicipio = "Guadalajara";
    private $DomTiendaEstado = "Jalisco";
    private $DomTiendaPais = "México";
    private $DomTiendaCodigoPostal = "44630";
    private $RFCTienda = "LHB1508052E7";
    private $CodMoneda = "MXN";
    private $DiasPago;
    private $PorcDescProntoPago;
    private $MontoDescProntoPago;
    private $CodDescuento;
    private $PorcDescuento;
    private $MontoDescuento = 550.86; // Importe descuento con IVA
    private $CantidadLineasFactura = 1; // Cantidad de Conceptos (incluye envío si lo hay)
    private $FechaVencimiento;
    private $CodZona;
    private $NumeroReceptor;
    private $CodVendedor = 1;
    private $NombreVendedor;
    private $ViaEmbarque;
    private $CondicionesPago;
    private $NumeroPedido;
    private $FechaPedido;
    private $LetrasMontoTotal;
    private $CantidadUnidades = 1; // Total Unidades en el Pedido (Sinenvio)
    private $CantidadEmpaques;
    private $GLNReceptor;
    private $GLNLugarExpide;
    private $IEPSId;
    private $Estatus;
    private $NumeroEmisor;
    private $MontoMerma;
    private $MontoSubTotalApIVA;
    private $Transportista;
    private $NumeroSolicitud;
    private $DescMoneda;
    private $Misc01;
    private $Misc02 = "BM";
    private $Misc03;
    private $Misc04;
    private $Misc05;
    private $Misc06 = 900025600; // No de orden Shopify
    private $Misc07;
    private $Misc08;
    private $Misc09;
    private $Misc10;
    private $Misc11;
    private $Misc12 = 601;
    private $Misc13 = "No identificado"; //Ultimos 4 digitos de la tarjeta
    private $Misc14;
    private $Misc15;
    private $Misc16;
    private $Misc17;
    private $Misc18;
    private $Misc19;
    private $Misc20;
    private $Misc21;
    private $Misc22;
    private $Misc23;
    private $Misc24;
    private $Misc25;
    private $Misc26;
    private $Misc27;
    private $Misc28;
    private $Misc29;
    private $Misc30;
    private $Misc31;
    public $Misc32 = "S01"; //G03=Venta, G02=Devoluciones
    private $Misc33;
    private $Misc34;
    private $Misc35;
    private $Misc36;
    private $Misc37;
    private $Misc38;
    private $Misc39;
    private $Misc40;
    private $Misc41;
    private $Misc42;
    private $Misc43;
    private $Misc44;
    private $Misc45;
    private $Misc46;
    private $Misc47;
    private $Misc48;
    private $Misc49;
    private $Misc50;
    private $PorcIVA = "0.160000";
    private $MontoIEPS;
    private $DocumentStatus = "ORIGINAL";
    private $DeliveryDate = "2021-08-21"; //Fecha de facturacion
    private $SpecialInstruction = "ZZZ";
    private $ReferenceIdentification = "BT";
    private $NumContrarecibo;
    private $FechaNumContrarecibo;
    private $ContactoCompras;
    private $CustomsGln;
    private $AlternateIdentificationGln;
    private $NombreAduana;
    private $NombreAduanaCiudad;
    private $FuncDivisa;
    private $TasaDivisa;
    private $RefTiempoPago;
    private $RefTerminoTiempoPago;
    private $DescuentoTipo = "ALLOWANCE_BY_PAYMENT_ON_TIME";
    private $IndicadorCargoDescuento = "ALLOWANCE_GLOBAL";
    private $InfCargoDescuento = "BILL_BACK";
    private $TipoEspecialDeServicio = "AJ";
    private $PorcentajeNoAplicado = 0;
    private $IdentificadorCargoDescuento = "ALLOWANCE";
    private $MontoTotalDescuentos = 550.86; // Descuentos de la orden
    private $MontoTotalPagar = 0; //Total de la orden
    private $AnoAprobacion;
    private $MotivoDescuento;
    private $MetodoPago = "PUE";
    public $EfectoComprobante = "I"; // I=Venta, E=Devolucion
    private $MontoTotalImpRetenidos = 0;
    private $MontoTotalImpTraslados = 0;

    //cfdi 4.0
    private $ReceptorRegimenFiscal = 612;
    private $CfdiExport = '01'; // 01 02 03 04
    private $Periodicity = '01';
    private $CfdiMonths = 12;
    private $CfdiYear = 1992;

// Footers privates
    private $ImpuestoTipoImpuesto = "TR";
    private $ImpuestoDescripcion = "IVA";
    private $ImpuestoMontoImporte = 0;
    private $ImpuestoTasa = "0.160000";

    //cfdi 4.0
    private $TaxableAmount= 0;

    public $UUID = '';

    //Montos de envio
    public $envioEstandarPrecioConIva = 70;
    private $envioEstandarPrecioSinIva = 0;
    private $envioEstandarTitulo = 'Servicios Logísticos';
    private $envioEstandarImpuesto = 0;

    private $fileName = '';
    private $concepts = [];

    public $idOrdenShopify = 0;
    public $ordenNum = '';
    public $ordenEmailClient = '';
    public $dateNow = '';
    public $contentB64 = '';
    public $contentTxt = '';
    public $tipoEnvio = '';

    public $statusProducto = '';

    private $addTotalQty = 0;
    private $subtotal = 0;
    private $total = 0;
    private $impuestos = 0;
    private $totalPagar = 0;

    private $totalProductosEnOrden = 0;

    public $prodctosToNC = [];

    public $uudRelacional = '';

    public $contentTxtSap = '';


    private function buildHeaders()
    {
        $mes = date('m');
        $year = date('Y');
        $this->CfdiMonths = $mes;
        $this->CfdiYear = $year;
        $formato = '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
            '%s|%s|%s|%s|%s|%s|%s|%s';
        $dataEdicomLine1 = sprintf($formato,
            $this->Folio,
            $this->NombreEmisor,
            $this->RFCEmisor,
            $this->DomEmisorCalle,
            $this->DomEmisorNoExterior,
            $this->DomEmisorNoInterior,
            $this->DomEmisorColonia,
            $this->DomEmisorLocalidad,
            $this->DomEmisorReferencia,
            $this->DomEmisorMunicipio,

            $this->DomEmisorEstado,
            $this->DomEmisorPais,
            $this->DomEmisorCodigoPostal,
            $this->TelEmisor,
            $this->DomSucursalCalle,
            $this->DomSucursalNoExterior,
            $this->DomSucursalNoInterior,
            $this->DomSucursalColonia,
            $this->DomSucursalLocalidad,
            $this->DomSucursalReferencia,

            $this->DomSucursalMunicipio,
            $this->DomSucursalEstado,
            $this->DomSucursalPais,
            $this->DomSucursalCodigoPostal,
            $this->TelSucursal,
            $this->Version,
            $this->SerieComprobante,
            $this->NumeroAprobacion,
            $this->FormaPago,
            $this->Fecha,

            $this->Hora,
            $this->DomLugarExpideCalle,
            $this->DomLugarExpideNoExterior,
            $this->DomLugarExpideNoInterior,
            $this->DomLugarExpideColonia,
            $this->DomLugarExpideLocalidad,
            $this->DomLugarExpideReferencia,
            $this->DomLugarExpideMunicipio,
            $this->DomLugarExpideEstado,
            $this->DomLugarExpidePais,

            $this->DomLugarExpideCodigoPostal,
            $this->NombreReceptor,
            $this->RFCReceptor,
            $this->DomReceptorCalle,
            $this->DomReceptorNoExterior,
            $this->DomReceptorNoInterior,
            $this->DomReceptorColonia,
            $this->DomReceptorLocalidad,
            $this->DomReceptorReferencia,
            $this->DomReceptorMunicipio,

            $this->DomReceptorEstado,
            $this->DomReceptorPais,
            $this->DomReceptorCodigoPostal,
            $this->MontoSubTotal,
            $this->MontoImpuestos,
            $this->MontoTotal,
            $this->Estado,
            $this->TipoCFD,
            $this->Notas,
            $this->Notas02,

            $this->Notas03,
            $this->TradingPartnerProv,
            $this->CalifTradingPartnerProv,
            $this->GLNProveedor,
            $this->NumeroFactura,
            $this->NumeroOrdenCompra,
            $this->FechaOrdenCompra,
            $this->NumeroProveedor,
            $this->GLNTienda,
            $this->NumeroTienda,

            $this->NombreTienda,
            $this->DomTiendaCalle,
            $this->DomTiendaNoExterior,
            $this->DomTiendaNoInterior,
            $this->DomTiendaColonia,
            $this->DomTiendaLocalidad,
            $this->DomTiendaReferencia,
            $this->DomTiendaMunicipio,
            $this->DomTiendaEstado,
            $this->DomTiendaPais,

            $this->DomTiendaCodigoPostal,
            $this->RFCTienda,
            $this->CodMoneda,
            $this->DiasPago,
            $this->PorcDescProntoPago,
            $this->MontoDescProntoPago,
            $this->CodDescuento,
            $this->PorcDescuento,
            $this->MontoDescuento,
            $this->CantidadLineasFactura,

            $this->FechaVencimiento,
            $this->CodZona,
            $this->NumeroReceptor,
            $this->CodVendedor,
            $this->NombreVendedor,
            $this->ViaEmbarque,
            $this->CondicionesPago,
            $this->NumeroPedido,
            $this->FechaPedido,
            $this->LetrasMontoTotal,

            $this->CantidadUnidades,
            $this->CantidadEmpaques,
            $this->GLNReceptor,
            $this->GLNLugarExpide,
            $this->IEPSId,
            $this->Estatus,
            $this->NumeroEmisor,
            $this->MontoMerma,
            $this->MontoSubTotalApIVA,
            $this->Transportista,

            $this->NumeroSolicitud,
            $this->DescMoneda,
            $this->Misc01,
            $this->Misc02,
            $this->Misc03,
            $this->Misc04,
            $this->Misc05,
            $this->Misc06,
            $this->Misc07,
            $this->Misc08,

            $this->Misc09,
            $this->Misc10,
            $this->Misc11,
            $this->Misc12,
            $this->Misc13,
            $this->Misc14,
            $this->Misc15,
            $this->Misc16,
            $this->Misc17,
            $this->Misc18,

            $this->Misc19,
            $this->Misc20,
            $this->Misc21,
            $this->Misc22,
            $this->Misc23,
            $this->Misc24,
            $this->Misc25,
            $this->Misc26,
            $this->Misc27,
            $this->Misc28,

            $this->Misc29,
            $this->Misc30,
            $this->Misc31,
            $this->Misc32,
            $this->Misc33,
            $this->Misc34,
            $this->Misc35,
            $this->Misc36,
            $this->Misc37,
            $this->Misc38,

            $this->Misc39,
            $this->Misc40,
            $this->Misc41,
            $this->Misc42,
            $this->Misc43,
            $this->Misc44,
            $this->Misc45,
            $this->Misc46,
            $this->Misc47,
            $this->Misc48,

            $this->Misc49,
            $this->Misc50,
            $this->PorcIVA,
            $this->MontoIEPS,
            $this->DocumentStatus,
            $this->DeliveryDate,
            $this->SpecialInstruction,
            $this->ReferenceIdentification,
            $this->NumContrarecibo,
            $this->FechaNumContrarecibo,

            $this->ContactoCompras,
            $this->CustomsGln,
            $this->AlternateIdentificationGln,
            $this->NombreAduana,
            $this->NombreAduanaCiudad,
            $this->FuncDivisa,
            $this->TasaDivisa,
            $this->RefTiempoPago,
            $this->RefTerminoTiempoPago,
            $this->DescuentoTipo,

            $this->IndicadorCargoDescuento,
            $this->InfCargoDescuento,
            $this->TipoEspecialDeServicio,
            $this->PorcentajeNoAplicado,
            $this->IdentificadorCargoDescuento,
            $this->MontoTotalDescuentos,
            $this->MontoTotalPagar,
            $this->AnoAprobacion,
            $this->MotivoDescuento,
            $this->MetodoPago,

            $this->EfectoComprobante,
            $this->MontoTotalImpRetenidos,
            $this->MontoTotalImpTraslados,
            $this->ReceptorRegimenFiscal,
            $this->CfdiExport,
            $this->Periodicity,
            $this->CfdiMonths,
            $this->CfdiYear
        );

        return $dataEdicomLine1;
    }

    private function buildConcept()
    {
        $totalPrecioUnitario = 0;
        $totalCantidad = 0;
        $totalDescuento = 0;
        $totalIva = 0;
        $prodSubtotal = 0;
        $prodTotal = 0;
        $prodTotalPagar = 0;
        $conceptLines = '';
        $basesTotales = 0;

        foreach ($this->concepts as $concept) {
            $privateVariantData = explode('/', $concept['varianTitle']);
            $color = trim($privateVariantData[0]);
            $talla = trim($privateVariantData[1]);
            $material = trim($privateVariantData[2]);

            $billLine = new EdicomBillLine();

            $this->envioEstandarPrecioSinIva = round(($this->envioEstandarPrecioConIva / 1.16), 2);
            $this->envioEstandarImpuesto = round(($this->envioEstandarPrecioSinIva * 0.16), 2);

            //--Valida si el descuento es cero---
            if ($concept['totalDiscount'] == '') {
                $lineaDescuento = 0;
            }
            $sku = explode('-', $concept['sku']);
            $codigo_alfa = trim($sku[0]);
            $billLine->Linea_Descripcion = $concept['title'];
            $billLine->Linea_Cantidad = $concept['cantidad'];
            $billLine->Linea_PrecioUnitario = round(($concept['importe'] / 1.16), 2); //Importe Linea / 1.16
            $billLine->Linea_Monto_Desc = round($concept['totalDiscount'] / 1.16, 2); //Descuento / 1.16
            $billLine->Linea_Importe = round(((round($concept['importe'] / 1.16, 2)) * $concept['cantidad']), 2); //Precio Unitario * Cantidad
            $billLine->Linea_Cod_Barras = $concept['sku'];
            // $billLine->Linea_Cod_Barras             = $codigo_alfa;
            $billLine->Linea_Cant_Empaques_Fac = $concept['cantidad'];
            $billLine->Linea_Cant_Empaques_Emb = $concept['cantidad'];
            $billLine->Linea_PrecioUnitario_SinDesc = round(($concept['importe'] / 1.16), 2); //Importe Linea / 1.16
            $billLine->Linea_Monto_IVA = round(((($concept['importe'] * $concept['cantidad']) / 1.16) * 0.16), 2) - round(((($concept['totalDiscount']) / 1.16) * 0.16), 2); //(Importe con iva - importe sin iva) - (descuento con iva - descuento sin iva)
            $billLine->Linea_Importe_ConImp = round((round($concept['importe'] * $concept['cantidad'], 2)) - $concept['totalDiscount'], 2); //(Importe - descuento) * cantidad
            $billLine->Linea_Misc04 = $concept['title'];
            $billLine->Linea_Misc05 = $talla;
            $billLine->Linea_Misc06 = $color;
            // $billLine->Linea_Misc37 = round(round((round($concept['importe'] * $concept['cantidad'], 2)) - $concept['totalDiscount'], 2) / 1.16, 2); //Importe con descuento / 1.16
            $base = round(round((round($concept['importe'] * $concept['cantidad'], 2)) - $concept['totalDiscount'], 2) / 1.16, 2);
            if ($base < 0) {
                $billLine->Linea_Misc37 = 0.04;
            } else {
                $billLine->Linea_Misc37 = round(round((round($concept['importe'] * $concept['cantidad'], 2)) - $concept['totalDiscount'], 2) / 1.16, 2); //Importe con descuento / 1.16
            }
            //$billLine->Linea_Misc37 = round(((($concept['importe'] * $concept['cantidad']) - $concept['totalDiscount'] ) / 1.16),2); //Importe con descuento / 1.16
            $billLine->Linea_Cod_EAN = $codigo_alfa;
            $billLine->Linea_NoIdentificacion = $codigo_alfa;
            $conceptLines .= $billLine->buildConcept();

            $basesTotales += (double) $billLine->Linea_Misc37;
            $totalPrecioUnitario += (double) $billLine->Linea_PrecioUnitario;
            $totalCantidad += (double) $billLine->Linea_Cantidad;
            $totalDescuento += (double) $billLine->Linea_Monto_Desc;
            $totalIva += (double) $billLine->Linea_Monto_IVA;

            $prodSubtotal += $billLine->Linea_PrecioUnitario * $billLine->Linea_Cantidad;
            $prodTotal += ($billLine->Linea_PrecioUnitario * $billLine->Linea_Cantidad) - $billLine->Linea_Monto_Desc;
            $prodTotalPagar += (($billLine->Linea_PrecioUnitario * $billLine->Linea_Cantidad) + $billLine->Linea_Monto_IVA) - $billLine->Linea_Monto_Desc;
        }

        $productLineTotals['orderSubtotal'] = $prodSubtotal;
        $productLineTotals['orderImpuesto'] = $totalIva;
        $productLineTotals['orderTotal'] = $prodTotal;
        $productLineTotals['orderDescuento'] = $totalDescuento;
        $productLineTotals['orderTotalPagar'] = $prodTotalPagar;
        $productLineTotals['orderTotalProductos'] = $totalCantidad;
        $productLineTotals['basesTotales'] = $basesTotales;

        if (($this->envioEstandarPrecioConIva > 0 && $this->tipoEnvio == 'CLIENTE' && $this->EfectoComprobante == 'I') || ($this->envioEstandarPrecioConIva > 0 && $this->tipoEnvio == 'CLIENTE' && $this->EfectoComprobante == 'E')) {
            $shipmentLine = new EdicomBillLine();
            $shipmentLine->Linea_Descripcion = $this->envioEstandarTitulo;
            $shipmentLine->Linea_Cantidad = 1;
            $shipmentLine->Linea_PrecioUnitario = $this->envioEstandarPrecioSinIva;
            $shipmentLine->Linea_Monto_Desc = 0;
            $shipmentLine->Linea_Importe = $this->envioEstandarPrecioSinIva;
            $shipmentLine->Linea_Cod_Barras = '';
            $shipmentLine->Linea_Cant_Empaques_Fac = 1;
            $shipmentLine->Linea_Cant_Empaques_Emb = 1;
            $shipmentLine->Linea_PrecioUnitario_SinDesc = $this->envioEstandarPrecioSinIva;
            $shipmentLine->Linea_Monto_IVA = $this->envioEstandarImpuesto;
            $shipmentLine->Linea_Importe_ConImp = $this->envioEstandarPrecioConIva;
            $shipmentLine->Linea_Misc04 = '';
            $shipmentLine->Linea_Misc05 = '';
            $shipmentLine->Linea_Misc06 = '';
            $shipmentLine->Linea_Misc37 = $this->envioEstandarPrecioSinIva;
            $shipmentLine->Linea_Misc28 = '80141703';
            $shipmentLine->Linea_Cod_EAN = '';
            $shipmentLine->Linea_NoIdentificacion = '';
            $conceptLines .= $shipmentLine->buildConcept();
        }
        $productLineTotals['conceptLines'] = $conceptLines;
        return $productLineTotals;
    }

    private function buildConceptSap()
    {
        $totalPrecioUnitario = 0;
        $totalCantidad = 0;
        $totalDescuento = 0;
        $totalIva = 0;
        $prodSubtotal = 0;
        $prodTotal = 0;
        $prodTotalPagar = 0;
        $conceptLines = '';
        $basesTotales = 0;

        foreach ($this->concepts as $concept) {
            $privateVariantData = explode('/', $concept['varianTitle']);
            $color = trim($privateVariantData[0]);
            $talla = trim($privateVariantData[1]);
            $material = trim($privateVariantData[2]);

            $billLine = new EdicomBillLine();

            $this->envioEstandarPrecioSinIva = round(($this->envioEstandarPrecioConIva / 1.16), 2);
            $this->envioEstandarImpuesto = round(($this->envioEstandarPrecioSinIva * 0.16), 2);

            //--Valida si el descuento es cero---
            if ($concept['descuento_base'] == '') {
                $lineaDescuento = 0;
            }
            $sku = explode('-', $concept['sku']);
            $codigo_alfa = trim($sku[0]);
            $billLine->Linea_Descripcion = $concept['title'];
            $billLine->Linea_Cantidad = $concept['cantidad'];
            $billLine->Linea_PrecioUnitario = round(($concept['precio_base'] / 1.16), 2); //Importe Linea / 1.16
            $billLine->Linea_Monto_Desc = round($concept['descuento_base'] / 1.16, 2); //Descuento / 1.16
            $billLine->Linea_Importe = round(((round($concept['precio_base'] / 1.16, 2)) * $concept['cantidad']), 2); //Precio Unitario * Cantidad
            $billLine->Linea_Cod_Barras = $concept['sku'];
            // $billLine->Linea_Cod_Barras             = $codigo_alfa;
            $billLine->Linea_Cant_Empaques_Fac = $concept['cantidad'];
            $billLine->Linea_Cant_Empaques_Emb = $concept['cantidad'];
            $billLine->Linea_PrecioUnitario_SinDesc = round(($concept['precio_base'] / 1.16), 2); //Importe Linea / 1.16
            $billLine->Linea_Monto_IVA = round(((($concept['precio_base'] * $concept['cantidad']) / 1.16) * 0.16), 2) - round(((($concept['descuento_base']) / 1.16) * 0.16), 2); //(Importe con iva - importe sin iva) - (descuento con iva - descuento sin iva)
            $billLine->Linea_Importe_ConImp = round((round($concept['precio_base'] * $concept['cantidad'], 2)) - $concept['descuento_base'], 2); //(Importe - descuento) * cantidad
            $billLine->Linea_Misc04 = $concept['title'];
            $billLine->Linea_Misc05 = $talla;
            $billLine->Linea_Misc06 = $color;
            // $billLine->Linea_Misc37 = round(round((round($concept['precio_base'] * $concept['cantidad'], 2)) - $concept['descuento_base'], 2) / 1.16, 2); //Importe con descuento / 1.16
            $base = round(round((round($concept['precio_base'] * $concept['cantidad'], 2)) - $concept['descuento_base'], 2) / 1.16, 2);
            if ($base < 0) {
                $billLine->Linea_Misc37 = 0.04;
            } else {
                $billLine->Linea_Misc37 = round(round((round($concept['precio_base'] * $concept['cantidad'], 2)) - $concept['descuento_base'], 2) / 1.16, 2); //Importe con descuento / 1.16
            }
            //$billLine->Linea_Misc37 = round(((($concept['importe'] * $concept['cantidad']) - $concept['totalDiscount'] ) / 1.16),2); //Importe con descuento / 1.16
            $billLine->Linea_Cod_EAN = $codigo_alfa;
            $billLine->Linea_NoIdentificacion = $codigo_alfa;
            $conceptLines .= $billLine->buildConcept();

            $basesTotales += (double) $billLine->Linea_Misc37;
            $totalPrecioUnitario += (double) $billLine->Linea_PrecioUnitario;
            $totalCantidad += (double) $billLine->Linea_Cantidad;
            $totalDescuento += (double) $billLine->Linea_Monto_Desc;
            $totalIva += (double) $billLine->Linea_Monto_IVA;

            $prodSubtotal += $billLine->Linea_PrecioUnitario * $billLine->Linea_Cantidad;
            $prodTotal += ($billLine->Linea_PrecioUnitario * $billLine->Linea_Cantidad) - $billLine->Linea_Monto_Desc;
            $prodTotalPagar += (($billLine->Linea_PrecioUnitario * $billLine->Linea_Cantidad) + $billLine->Linea_Monto_IVA) - $billLine->Linea_Monto_Desc;
        }

        $productLineTotals['orderSubtotal'] = $prodSubtotal;
        $productLineTotals['orderImpuesto'] = $totalIva;
        $productLineTotals['orderTotal'] = $prodTotal;
        $productLineTotals['orderDescuento'] = $totalDescuento;
        $productLineTotals['orderTotalPagar'] = $prodTotalPagar;
        $productLineTotals['orderTotalProductos'] = $totalCantidad;
        $productLineTotals['basesTotales'] = $basesTotales;
        // Log::debug('----------calculo producto----------');
        //     Log::debug('Subtotal: '.$productLineTotals['orderSubtotal']);
        //     Log::debug('Impuestos: '.$productLineTotals['orderImpuesto']);
        //     Log::debug('Monto Descuento: '.$productLineTotals['orderDescuento']);
        //     Log::debug('Monto Total: '.$productLineTotals['orderTotal']);
        //     Log::debug('monto Total Pagar: '.$productLineTotals['orderTotalPagar']);
        //     Log::debug('----------calculo producto----------');

        if (($this->envioEstandarPrecioConIva > 0 && $this->tipoEnvio == 'CLIENTE' && $this->EfectoComprobante == 'I') || ($this->envioEstandarPrecioConIva > 0 && $this->tipoEnvio == 'CLIENTE' && $this->EfectoComprobante == 'E')) {
            $shipmentLine = new EdicomBillLine();
            $shipmentLine->Linea_Descripcion = $this->envioEstandarTitulo;
            $shipmentLine->Linea_Cantidad = 1;
            $shipmentLine->Linea_PrecioUnitario = $this->envioEstandarPrecioSinIva;
            $shipmentLine->Linea_Monto_Desc = 0;
            $shipmentLine->Linea_Importe = $this->envioEstandarPrecioSinIva;
            $shipmentLine->Linea_Cod_Barras = '';
            $shipmentLine->Linea_Cant_Empaques_Fac = 1;
            $shipmentLine->Linea_Cant_Empaques_Emb = 1;
            $shipmentLine->Linea_PrecioUnitario_SinDesc = $this->envioEstandarPrecioSinIva;
            $shipmentLine->Linea_Monto_IVA = $this->envioEstandarImpuesto;
            $shipmentLine->Linea_Importe_ConImp = $this->envioEstandarPrecioConIva;
            $shipmentLine->Linea_Misc04 = '';
            $shipmentLine->Linea_Misc05 = '';
            $shipmentLine->Linea_Misc06 = '';
            $shipmentLine->Linea_Misc37 = $this->envioEstandarPrecioSinIva;
            $shipmentLine->Linea_Misc28 = '80141703';
            $shipmentLine->Linea_Cod_EAN = '';
            $shipmentLine->Linea_NoIdentificacion = '';
            $conceptLines .= $shipmentLine->buildConcept();
        }
        $productLineTotals['conceptLines'] = $conceptLines;
        return $productLineTotals;
    }

    private function buildFooter()
    {
        $formato = '%s|%s|%s|%s|%s';

        $dataEdicomLine3 = sprintf($formato,
            $this->ImpuestoTipoImpuesto,
            $this->ImpuestoDescripcion,
            $this->ImpuestoMontoImporte,
            $this->ImpuestoTasa,
            $this->TaxableAmount
        );

        return $dataEdicomLine3;
    }

    private function buildFooterNc()
    {
        $formato = '¬%s|%s';

        $dataEdicomLine3 = sprintf($formato,
            'CFDI',
            $this->UUID
        );

        return $dataEdicomLine3;
    }

    //---- Datos de la factura general ------
    private function createFileContent()
    {
        $orderData = new OrderDaoController();
        $orderData->idOrdenShopify = $this->idOrdenShopify;
        $resultOrder = $orderData->getOrderBillingDataById();
        // dd($this);
        $this->tipoEnvio = $orderData->shippingMethod;
        $this->Fecha = date('Y-m-d');
        $this->Hora = date('H:i:s');
        $this->DeliveryDate = date('Y-m-d');

        /** @TODO */
        // error_log("SysLog|" . date('Y-m-d H:i:s') . "|" . "FACT002" . "|Factura:{$orderData->requiereFactura} - Tipo Envio: {$this->tipoEnvio} - Nombre: {$orderData->nombre}" . PHP_EOL, 3, APP_LOG_FILE);
        Log::debug("Factura:{$orderData->requiereFactura} - Tipo Envio: {$this->tipoEnvio} - Nombre: {$orderData->nombre}");
        if ($orderData->requiereFactura == 'Si' && $this->tipoEnvio == 'CLIENTE') {

            $billRfc = Str::upper($orderData->clienteRfc);
            $billNombreReceptor = Str::upper($this->quitar_tildes($orderData->nombre . ' ' . $orderData->apellido1 . ' ' . $orderData->apellido2));
            $billDomReceptorcalle = $orderData->calle;
            $billDomReceptorNoExterior = $orderData->noExterior;
            $billDomReceptorNoInterior = $orderData->noInterior;
            $billDomReceptorColonia = $orderData->colonia;
            $billDomReceptorLocalidad = '';
            $billDomReceptorReferencia = '';
            $billDomReceptorMunicipio = $orderData->poblacion;
            $billDomReceptorEstado = $orderData->provincia;
            $billDomReceptorPais = "México";
            $billDomReceptorCodigoPostal = $orderData->codigoPostal;
            $billCFDIReceptor = $orderData->cfdi_code;
        } elseif ($orderData->requiereFactura == 'Si' && $this->tipoEnvio == 'TIENDA' && strpos($orderData->nombre, 'S2S') > -1) {
            $billRfc = Str::upper($orderData->clienteRfc);
            $billNombreReceptor = "";
            $billDomReceptorcalle = "";
            $billDomReceptorNoExterior = "";
            $billDomReceptorNoInterior = "";
            $billDomReceptorColonia = "";
            $billDomReceptorLocalidad = "";
            $billDomReceptorReferencia = "";
            $billDomReceptorMunicipio = "";
            $billDomReceptorEstado = "";
            $billDomReceptorPais = "";
            $billDomReceptorCodigoPostal = "";
            $billCFDIReceptor = 616;
        } elseif ($orderData->requiereFactura == 'Si' && $this->tipoEnvio == 'TIENDA' && strpos($orderData->nombre, 'S2S') <= 0) {
            $billRfc = Str::upper($orderData->clienteRfc);
            $billNombreReceptor = Str::upper($this->quitar_tildes($orderData->nombre . ' ' . $orderData->apellido1 . ' ' . $orderData->apellido2));
            $billDomReceptorcalle = $orderData->calle;
            $billDomReceptorNoExterior = $orderData->noExterior;
            $billDomReceptorNoInterior = $orderData->noInterior;
            $billDomReceptorColonia = $orderData->colonia;
            $billDomReceptorLocalidad = '';
            $billDomReceptorReferencia = '';
            $billDomReceptorMunicipio = $orderData->poblacion;
            $billDomReceptorEstado = $orderData->provincia;
            $billDomReceptorPais = "México";
            $billDomReceptorCodigoPostal = $orderData->codigoPostal;
            $billCFDIReceptor = 616;
        } else {
            $billRfc = 'XAXX010101000';
            $billNombreReceptor = "PUBLICO EN GENERAL";
            $billDomReceptorcalle = "Ontario";
            $billDomReceptorNoExterior = '1090';
            $billDomReceptorNoInterior = 'Piso 3';
            $billDomReceptorColonia = "Colonia Providencia 1a, 2a y 3a sección";
            $billDomReceptorLocalidad = '';
            $billDomReceptorReferencia = '';
            $billDomReceptorMunicipio = "Guadalajara";
            $billDomReceptorEstado = "Jalisco";
            $billDomReceptorPais = "México";
            $billDomReceptorCodigoPostal = '44690';
            $billCFDIReceptor = 616;
        }

        if ($this->TipoCFD == 'NC') {
            $productsOrderData = new OrderDaoController;
            $productsOrderData->idOrdenShopify = $this->idOrdenShopify;
            $productsOrderData->getProductsOrder($this->Folio);
            // $this->prodctosToNC = $productsOrderData->productsData;
        }else{
            $productsOrderData = new OrderDaoController;
            $productsOrderData->idOrdenShopify = $this->idOrdenShopify;
            $productsOrderData->getProductsOrderByOrderId($this->statusProducto);
        }

        // $productsOrderData = new OrderDaoController;
        // $productsOrderData->idOrdenShopify = $this->idOrdenShopify;
        // $productsOrderData->getProductsOrderByOrderId($this->statusProducto);
        //---Construye los conceptos--------
        $this->concepts = $productsOrderData->productsData;
        $dataConcepts = $this->buildConcept();
        $dataConceptsSap = $this->buildConceptSap();


        $fileContentsEdicom = $this->fileContentsEdicom(
            $dataConcepts,
            $orderData,
            $billRfc,
            $billNombreReceptor,
            $billDomReceptorcalle,
            $billDomReceptorNoExterior,
            $billDomReceptorNoInterior,
            $billDomReceptorColonia,
            $billDomReceptorLocalidad,
            $billDomReceptorReferencia,
            $billDomReceptorMunicipio,
            $billDomReceptorEstado,
            $billDomReceptorPais,
            $billDomReceptorCodigoPostal,
            $billCFDIReceptor
        );


        $fileContentsSap = $this->fileContentsSap(
                $dataConceptsSap,
                $orderData,
                $billRfc,
                $billNombreReceptor,
                $billDomReceptorcalle,
                $billDomReceptorNoExterior,
                $billDomReceptorNoInterior,
                $billDomReceptorColonia,
                $billDomReceptorLocalidad,
                $billDomReceptorReferencia,
                $billDomReceptorMunicipio,
                $billDomReceptorEstado,
                $billDomReceptorPais,
                $billDomReceptorCodigoPostal,
                $billCFDIReceptor
            );

        return true;


    }

    public function createBill()
    {
        $folio = '';
        // Log::debug($this->SerieComprobante . ' ' . $this->idOrdenShopify . ' ' . $this->ordenNum . ' ' . $this->RFCReceptor . ' ' . $this->ordenEmailClient);
        try {
            // dd($this);
            if($this->TipoCFD == 'NC'){
                $validate = EdicomBill::where('id_orden', '=', $this->idOrdenShopify)->where('serie', '=', config('app.SERIE_NC'))->where('folio', '=', $this->Folio)->first();
            }else{
                $validate = EdicomBill::where('id_orden', '=', $this->idOrdenShopify)->where('serie', '=', $this->SerieComprobante)->first();
            }

            if (!$validate) {
                Log::debug('no existe');
                $dateNow = date('Y-m-d H:i:s');
            }else{
                Log::debug('si existe');
                $dateNow = $validate->created;
                $folio = $validate->folio;
            }

            $order = EdicomBill::updateOrCreate(
                ['id_orden' => $this->idOrdenShopify, 'serie' => $this->SerieComprobante, 'folio'=> $folio],
                [
                    'serie' => $this->SerieComprobante,
                    'id_orden' => $this->idOrdenShopify,
                    'orden_num' => $this->ordenNum,
                    'bill_status' => 'generated',
                    'rfc' => $this->RFCReceptor,
                    'email' => $this->ordenEmailClient,
                    'created' => $dateNow
                ]
            );
            $this->Folio = $order->folio;
            // dd($this);
            // dd($this->Folio);
            // Log::debug($edicomBill);
            // $this->Folio = $edicomBill->folio;
            // $this->Folio = 1383;
            $responseCreateContent = $this->createFileContent();
            // $responseCreateContent = false;

            // $responseCreateContent = false;
            // Log::debug('Retornando respuesta creacion de archivo ');

            $updateEdicomBill = EdicomBill::where('serie', '=', $this->SerieComprobante)
            ->where('folio', '=', $this->Folio)
            ->update([
                'file_contents' => $this->contentTxt,
                'edi_contents' => $this->contentTxtSap,
                'file_b64' => $this->contentB64,
            ]);

            if ($responseCreateContent) {
                $edicomResponse = $this->sendEdicomBill($this->contentB64);
                // $edicomResponse = true;
                if ($edicomResponse) {
                    // $updateEdicomBill = EdicomBill::where('serie', '=', $this->SerieComprobante)
                    //                                 ->where('folio', '=', $this->Folio)
                    //                                 ->update([
                    //                                     'file_contents' => $this->contentTxt,
                    //                                     'file_b64' => $this->contentB64,
                    //                                 ]);
                    // $updateEdicomBill = DB::table('edicom_bills')
                    //     ->where('serie', '=', $this->SerieComprobante)
                    //     ->where('folio', '=', $this->Folio)
                    //     ->update([
                    //         'file_contents' => $this->contentTxt,
                    //         'file_b64' => $this->contentB64,
                    //     ]);
                        Log::debug($updateEdicomBill);
                    if ($updateEdicomBill) {
                        Log::debug('Se actualizo la data en bd, insertando la data de edicom.');
                        return true;
                    } else {
                        Log::debug('Edicom Error: Update Bill in BDD');
                        return false;
                    }
                }
            } else {
                Log::debug('no se pudo crear el contenido ni enviar a edicom');
                return false;
            }

        } catch (\Exception $e) {
            Log::debug('Edicom Error: ' . $e->getMessage());
            return false;
        }
    }

    private function sendEdicomBill($b64Data)
    {

        $ediwin_URL = config('app.ediwin_url');
        $client = new SoapClient($ediwin_URL, array('trace' => 1));
        $client->registerSession(config('app.ediwin_usr'), config('app.ediwin_passwd'), config('app.ediwin_domain'), config('app.ediwin_usr_type'));
        try {
            $b64Decode = base64_decode($b64Data);
            $ediwinResult = $client->publishDocumentB64($b64Decode, 2, 'MAPA_INTEGRACION_CFDI_TPV_LIFE');
            $response_xml = $client->__getLastResponse();
            Log::debug('Ediwin Response: ' . $response_xml);

            $uuid_edicom = '';
            if ($ediwinResult) {
                // $uuid_edicom = getUuid($ediwinResult);

               EdicomUid::dispatch($this->idOrdenShopify, $this->SerieComprobante, $this->Folio, $ediwinResult)->delay(now()->addMinutes(1));

            }

            sleep(5);

            $updateEdicomBill = DB::table('edicom_bills')
                ->where('serie', '=', $this->SerieComprobante)
                ->where('folio', '=', $this->Folio)
                ->update([
                    'bill_status' => 'confirmed',
                    'verification_code' => $ediwinResult,
                    'error_msg' => '',
                    'uuid_factura' => $uuid_edicom,
                    'uuid_factura_relacional' => $this->uudRelacional
                ]);



            return true;

        } catch (\Exception $e) {
            Log::debug('Error al envia a edicom ' . $e->getMessage());
            $response_xml = $client->__getLastResponse();
            $updateEdicomBill = DB::table('edicom_bills')
                ->where('serie', $this->SerieComprobante)
                ->where('folio', $this->Folio)
                ->update([
                    'bill_status' => 'error',
                    'error_msg' => $response_xml,
                ]);
            return false;
        }

    }

    private function fileContentsEdicom(
        $dataConcepts,
        $orderData,
        $billRfc,
        $billNombreReceptor,
        $billDomReceptorcalle,
        $billDomReceptorNoExterior,
        $billDomReceptorNoInterior,
        $billDomReceptorColonia,
        $billDomReceptorLocalidad,
        $billDomReceptorReferencia,
        $billDomReceptorMunicipio,
        $billDomReceptorEstado,
        $billDomReceptorPais,
        $billDomReceptorCodigoPostal,
        $billCFDIReceptor
    ){


        $this->totalProductosEnOrden = $dataConcepts['orderTotalProductos'];
        $this->NumeroPedido = $orderData->orderNum;
        $this->FechaPedido = $orderData->fechaPedido;
        $this->Misc06 = $orderData->orderNum;
        $this->Misc01 = $orderData->emailEnvio; // agregado

        // if ($this->EfectoComprobante == 'E') {
        //     $this->envioEstandarPrecioSinIva = 0;
        //     $this->envioEstandarPrecioConIva = 0;
        // } else {
        $this->envioEstandarPrecioSinIva = round(($this->envioEstandarPrecioConIva / 1.16), 2);
        $this->envioEstandarImpuesto = round(($this->envioEstandarPrecioSinIva * 0.16), 2);
        // }
        // $totalProductsAmount = new OrderDaoController;
        // $totalProductsAmount->idOrdenShopify = $this->idOrdenShopify;
        // $totalProductsAmount->getTotalAmountByProductStatus();
        // dd($totalProductsAmount);
        //TODO: Crear metodo elegante para validar envio y sumar a cantidad de productos
        if ($this->envioEstandarPrecioConIva > 0 && $this->tipoEnvio == 'CLIENTE') {
            $basesTotales =  round($this->envioEstandarPrecioSinIva + $dataConcepts['basesTotales'], 2);
            $this->addTotalQty = $this->totalProductosEnOrden + 1; // +1 por el concepto de envio
            $this->subtotal = round(($dataConcepts['orderSubtotal'] + $this->envioEstandarPrecioSinIva), 2);
            $this->impuestos = round(($dataConcepts['orderImpuesto'] + ($this->envioEstandarPrecioSinIva * 0.16)), 2);
            $this->totalPagar = round(($this->subtotal + $this->impuestos - $dataConcepts['orderDescuento']), 2);
            $this->total = round((($dataConcepts['orderTotal'] + $this->envioEstandarPrecioConIva) / 1.16), 2);
        } else {
            $basesTotales = $dataConcepts['basesTotales'];
            $this->addTotalQty = $this->totalProductosEnOrden;
            $this->subtotal = round($dataConcepts['orderSubtotal'], 2);
            $this->impuestos = round($dataConcepts['orderImpuesto'], 2);
            $this->totalPagar = round($this->subtotal + $this->impuestos - $dataConcepts['orderDescuento'], 2);
            $this->total = round(($dataConcepts['orderTotal'] / 1.16), 2);
        }

        $this->NombreReceptor = $billNombreReceptor;
        $this->RFCReceptor = $billRfc;
        $this->ReceptorRegimenFiscal = $billCFDIReceptor;
        $this->DomReceptorCalle = $billDomReceptorcalle;
        $this->DomReceptorNoExterior = $billDomReceptorNoExterior;
        $this->DomReceptorNoInterior = $billDomReceptorNoInterior;
        $this->DomReceptorColonia = $billDomReceptorColonia;
        $this->DomReceptorLocalidad = '';
        $this->DomReceptorReferencia = '';
        $this->DomReceptorMunicipio = $billDomReceptorMunicipio;
        $this->DomReceptorEstado = $billDomReceptorEstado;
        $this->DomReceptorPais = $billDomReceptorPais;
        $this->DomReceptorCodigoPostal = $billDomReceptorCodigoPostal;
        $this->MontoSubTotal = $this->subtotal;
        $this->MontoImpuestos = $this->impuestos;
        $this->MontoTotal = $this->totalPagar;
        $this->TaxableAmount = round( $basesTotales, 2);
        $this->MontoDescuento = round($dataConcepts['orderDescuento'], 2);
        $this->MontoTotalDescuentos = round($dataConcepts['orderDescuento'], 2);
        $this->MontoTotalPagar = $this->totalPagar;

        $this->fileName = 'pedido_' . $this->SerieComprobante . '-' . $this->Folio;

        $this->CantidadLineasFactura = $this->addTotalQty;
        $this->CantidadUnidades = $this->addTotalQty;
        //$this->concepts = $productsOrderData->productsData;
        $this->ImpuestoMontoImporte = $this->impuestos;

        $formatter = new NumeroALetras();

        $this->LetrasMontoTotal = $formatter->toInvoice($this->totalPagar, 2, 'PESOS');
        // $this->LetrasMontoTotal = convertir($this->totalPagar);

        //$this->totalProductosEnOrden = $this->addTotalQty;
        if($this->TipoCFD == 'NC'){
            $this->Misc30 = '01';
            $dataFooterNC = $this->buildFooterNc();
        }else{
            $dataFooterNC = '';
        }
        $dataHeaders = $this->buildHeaders();
        $dataFooter = $this->buildFooter();

        //$formato = '~%s'.PHP_EOL.PHP_EOL.'¬%s'.PHP_EOL.PHP_EOL.'¬%s';
        //$formato = '~%s'.chr(13).chr(10).'¬%s'.chr(13).chr(10).'¬%s';
        $formato = '~%s' . chr(13) . chr(10) . '%s¬%s' . chr(13) . chr(10) .'%s';
        //$formato = '~%s¬%s¬%s';

        $dataEdicom = sprintf($formato,
            $dataHeaders,
            $dataConcepts['conceptLines'],
            $dataFooter,
            $dataFooterNC
        );

        // TODO: Crear validación de existencia de directorio y permisos
        //file_put_contents(__DIR__ .'//storage//'.$this->fileName.'.txt',$dataEdicom);

        // $string_encoded = iconv('UTF-8', 'Windows-1252//TRANSLIT', $dataEdicom);
        // $public_dir = public_path();

        // $zip = new ZipArchive;
        // if ($zip->open($public_dir . '/storage/' . $this->fileName . '.zip', ZipArchive::CREATE) === true) {
        //     $zip->addFromString($this->fileName . '.txt', $string_encoded);
        //     $zip->close();
        // }

        // //$fileContents = file_get_contents(__DIR__ .'//storage//'.$this->fileName.'.zip');
        // $fp = fopen($public_dir . '/storage/' . $this->fileName . '.zip', "rb");
        // $binary = fread($fp, filesize($public_dir . '/storage/' . $this->fileName . '.zip'));
        $this->contentTxt = $dataEdicom;
        return true;
    }

    private function fileContentsSap(
        $dataConcepts,
        $orderData,
        $billRfc,
        $billNombreReceptor,
        $billDomReceptorcalle,
        $billDomReceptorNoExterior,
        $billDomReceptorNoInterior,
        $billDomReceptorColonia,
        $billDomReceptorLocalidad,
        $billDomReceptorReferencia,
        $billDomReceptorMunicipio,
        $billDomReceptorEstado,
        $billDomReceptorPais,
        $billDomReceptorCodigoPostal,
        $billCFDIReceptor
    ){
        $this->totalProductosEnOrden = $dataConcepts['orderTotalProductos'];
        $this->NumeroPedido = $orderData->orderNum;
        $this->FechaPedido = $orderData->fechaPedido;
        $this->Misc06 = $orderData->orderNum;
        $this->Misc01 = $orderData->emailEnvio; // agregado

        // if ($this->EfectoComprobante == 'E') {
        //     $this->envioEstandarPrecioSinIva = 0;
        //     $this->envioEstandarPrecioConIva = 0;
        // } else {
        $this->envioEstandarPrecioSinIva = round(($this->envioEstandarPrecioConIva / 1.16), 2);
        $this->envioEstandarImpuesto = round(($this->envioEstandarPrecioSinIva * 0.16), 2);
        // }
        // $totalProductsAmount = new OrderDaoController;
        // $totalProductsAmount->idOrdenShopify = $this->idOrdenShopify;
        // $totalProductsAmount->getTotalAmountByProductStatus();
        // dd($totalProductsAmount);
        //TODO: Crear metodo elegante para validar envio y sumar a cantidad de productos
        if ($this->envioEstandarPrecioConIva > 0 && $this->tipoEnvio == 'CLIENTE') {
            $basesTotales =  round($this->envioEstandarPrecioSinIva + $dataConcepts['basesTotales'], 2);
            $this->addTotalQty = $this->totalProductosEnOrden + 1; // +1 por el concepto de envio
            $this->subtotal = round(($dataConcepts['orderSubtotal'] + $this->envioEstandarPrecioSinIva), 2);
            $this->impuestos = round(($dataConcepts['orderImpuesto'] + ($this->envioEstandarPrecioSinIva * 0.16)), 2);
            $this->totalPagar = round(($this->subtotal + $this->impuestos - $dataConcepts['orderDescuento']), 2);
            $this->total = round((($dataConcepts['orderTotal'] + $this->envioEstandarPrecioConIva) / 1.16), 2);
        } else {
            $basesTotales = $dataConcepts['basesTotales'];
            $this->addTotalQty = $this->totalProductosEnOrden;
            $this->subtotal = round($dataConcepts['orderSubtotal'], 2);
            $this->impuestos = round($dataConcepts['orderImpuesto'], 2);
            $this->totalPagar = round($this->subtotal + $this->impuestos - $dataConcepts['orderDescuento'], 2);
            $this->total = round(($dataConcepts['orderTotal'] / 1.16), 2);
        }

        $this->NombreReceptor = $billNombreReceptor;
        $this->RFCReceptor = $billRfc;
        $this->ReceptorRegimenFiscal = $billCFDIReceptor;
        $this->DomReceptorCalle = $billDomReceptorcalle;
        $this->DomReceptorNoExterior = $billDomReceptorNoExterior;
        $this->DomReceptorNoInterior = $billDomReceptorNoInterior;
        $this->DomReceptorColonia = $billDomReceptorColonia;
        $this->DomReceptorLocalidad = '';
        $this->DomReceptorReferencia = '';
        $this->DomReceptorMunicipio = $billDomReceptorMunicipio;
        $this->DomReceptorEstado = $billDomReceptorEstado;
        $this->DomReceptorPais = $billDomReceptorPais;
        $this->DomReceptorCodigoPostal = $billDomReceptorCodigoPostal;
        $this->MontoSubTotal = $this->subtotal;
        $this->MontoImpuestos = $this->impuestos;
        $this->MontoTotal = $this->totalPagar;
        $this->TaxableAmount = round( $basesTotales, 2);
        $this->MontoDescuento = round($dataConcepts['orderDescuento'], 2);
        $this->MontoTotalDescuentos = round($dataConcepts['orderDescuento'], 2);
        $this->MontoTotalPagar = $this->totalPagar;

        $this->fileName = 'pedido_' . $this->SerieComprobante . '-' . $this->Folio;

        $this->CantidadLineasFactura = $this->addTotalQty;
        $this->CantidadUnidades = $this->addTotalQty;
        //$this->concepts = $productsOrderData->productsData;
        $this->ImpuestoMontoImporte = $this->impuestos;

        $formatter = new NumeroALetras();

        $this->LetrasMontoTotal = $formatter->toInvoice($this->totalPagar, 2, 'PESOS');
        // $this->LetrasMontoTotal = convertir($this->totalPagar);

        //$this->totalProductosEnOrden = $this->addTotalQty;
        if($this->TipoCFD == 'NC'){
            $this->Misc30 = '01';
            $dataFooterNC = $this->buildFooterNc();
        }else{
            $dataFooterNC = '';
        }
        $dataHeaders = $this->buildHeaders();
        $dataFooter = $this->buildFooter();

        //$formato = '~%s'.PHP_EOL.PHP_EOL.'¬%s'.PHP_EOL.PHP_EOL.'¬%s';
        //$formato = '~%s'.chr(13).chr(10).'¬%s'.chr(13).chr(10).'¬%s';
        $formato = '~%s' . chr(13) . chr(10) . '%s¬%s' . chr(13) . chr(10) .'%s';
        //$formato = '~%s¬%s¬%s';

        $dataEdicom = sprintf($formato,
            $dataHeaders,
            $dataConcepts['conceptLines'],
            $dataFooter,
            $dataFooterNC
        );

        // TODO: Crear validación de existencia de directorio y permisos
        //file_put_contents(__DIR__ .'//storage//'.$this->fileName.'.txt',$dataEdicom);

        $string_encoded = iconv('UTF-8', 'Windows-1252//TRANSLIT', $dataEdicom);
        $public_dir = public_path();

        $zip = new ZipArchive;
        if ($zip->open($public_dir . '/storage/' . $this->fileName . '.zip', ZipArchive::CREATE) === true) {
            $zip->addFromString($this->fileName . '.txt', $string_encoded);
            $zip->close();
        }

        //$fileContents = file_get_contents(__DIR__ .'//storage//'.$this->fileName.'.zip');
        $fp = fopen($public_dir . '/storage/' . $this->fileName . '.zip', "rb");
        $binary = fread($fp, filesize($public_dir . '/storage/' . $this->fileName . '.zip'));
        $this->contentTxtSap = $dataEdicom;
        $this->contentB64 = base64_encode($binary);
        return true;
    }


    function quitar_tildes($cadena){


        // $cadena = utf8_encode($cadena);


        $cadena = str_replace(
        array('á', 'à', 'â', 'ª', 'Á', 'À', 'Â'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $cadena
        );

        $cadena = str_replace(
        array('é', 'è', 'ê', 'É', 'È', 'Ê'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $cadena );

        $cadena = str_replace(
        array('í', 'ì', 'î', 'Í', 'Ì', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $cadena );

        $cadena = str_replace(
        array('ó', 'ò', 'ô', 'Ó', 'Ò', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $cadena );

        $cadena = str_replace(
        array('ú', 'ù', 'û', 'Ú', 'Ù', 'Û'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $cadena );

        $cadena = str_replace(
        array('ç', 'Ç'),
        array('c', 'C'),
        $cadena
        );

        return $cadena;
        }

}
