<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;

class CancelationController extends Controller
{
    public function Cancelar()
    {
        Artisan::call('cancelation:cron');
        return 'OK';
    }

    public function pendingInvoices()
    {
        Artisan::call('edicom:validation');
        return 'OK';
    }

    public function changeTypeInvoice()
    {
        Artisan::call('invoice:change-type-customer');
        return 'OK';
    }

}
