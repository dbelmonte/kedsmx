<?php

namespace App\Http\Controllers;

use App\Order;
use SoapClient;
use App\Product;
use Carbon\Carbon;
use App\EdicomBill;
use App\OrderAddress;
use App\OrderProductNc;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\EdicomClass;
use Mtownsend\XmlToArray\XmlToArray;

//use App\Http\Controllers\EdicomBillLine;

class OrderController extends Controller
{

    public function index()
    {
        return view('orders');
    }

    public function getOrders()
    {
        $orders = Order::query();
        // $orders = Order::pendingOrders()->get();
        return Datatables::of($orders)
            ->addColumn('full_name', function ($order) {
                return $order->nombre . ' ' . $order->apellido_2;
            })
            ->addColumn('options', function ($order) {
                $action = '<a href="' . route('order.show', $order->id_orden) . '" class="btn btn-icon btn-sm btn-light" taget="_blank"><i class="ri-eye-fill"></i></a>';
                return $action;
            })
            ->rawColumns(['options'])
            ->make(true);
    }

    public function show($id_orden)
    {
        $order = Order::findOrFail($id_orden);
        $products = Product::where('id_orden', $id_orden)->get();
        $addressess = OrderAddress::where('id_orden', $id_orden)->get();
        $edicom = EdicomBill::where('id_orden', $id_orden)->get();
        // $billing = OrderAddress::where('id_orden', $id_orden)->where('tipo', 'billing')->first();
        return view('orders.detail', ['products' => $products, 'order' => $order, 'addressess' => $addressess, 'edicom'=>$edicom]);
    }

    public function facturar(Request $request)
    {
        try {
            $id_orden = $request->input('id');
            $opcion = $request->input('option');
            $order = Order::findOrFail($id_orden);
            if ($order->payment_method == 'Bank Deposit') {
                $billPaymentMethod = '03';
            } else {
                $billPaymentMethod = '04';
            }
            $edicomBill = new EdicomClass();
            $edicomBill->SerieComprobante = 'ABK';
            $edicomBill->idOrdenShopify = $order->id_orden;
            $edicomBill->ordenNum = $order->orden_num;
            $edicomBill->RFCReceptor = $order->cliente_rfc;
            $edicomBill->ordenEmailClient = $order->email_envio;
            $edicomBill->statusProducto = 'preparado';
            $edicomBill->EfectoComprobante = 'I';
            $edicomBill->FormaPago = $billPaymentMethod;
            $edicomBill->envioEstandarPrecioConIva = floatval($order->shipping_price);
            $edicomBill->Misc32 = $opcion;
            $edicomBill->createBill();

            Log::debug('Generacion de factura exitosa');
            return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'Factura generada correctamente'], 200);
        } catch (\Throwable $th) {
            return response()->json(['ok' => false, 'icon' => 'error', 'msg' => 'Error al general la factura'], 200);
        }
    }

    public function cancelOrder(Request $request)
    {
        try {
            $id_orden = $request->input('id');
            $opcion = $request->input('opcion');
            $order = Order::findOrFail($id_orden);
            if ($order->payment_method == 'Bank Deposit') {
                $billPaymentMethod = '03';
            } else {
                $billPaymentMethod = '04';
            }
            $edicomBill = new EdicomClass();
            $edicomBill->SerieComprobante = 'BBK';
            $edicomBill->idOrdenShopify = $order->id_orden;
            $edicomBill->ordenNum = $order->orden_num;
            $edicomBill->RFCReceptor = $order->cliente_rfc;
            $edicomBill->ordenEmailClient = $order->email_envio;
            $edicomBill->statusProducto = 'nota_credito';
            $edicomBill->EfectoComprobante = 'E';
            $edicomBill->TipoCFD = 'NC';
            $edicomBill->FormaPago = $billPaymentMethod;
            if ($opcion == 'Si') {
                $edicomBill->envioEstandarPrecioConIva = floatval($order->shipping_price);
            } else {
                $edicomBill->envioEstandarPrecioConIva = 0;
            }
            $edicomBill->createBill();
            Log::debug('Generacion de nota de credito exitosa');
            return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'NC generada correctamente'], 200);
        } catch (\Throwable $th) {
            return response()->json(['ok' => false, 'icon' => 'error', 'msg' => 'Error al general la NC'], 200);
        }
    }

    public function sendToHermes(Request $request)
    {
        // try {
        $id_orden = $request->input('id');
        // $order = Order::findOrFail(2212685807750);
        $opcion = $request->input('opcion');
        if ($opcion == 'CANCELACION') {
            $tipo = 'CANCELACION';
        } elseif ($opcion == 'RESERVA') {
            $tipo = 'RESERVA';
        } else {
            $tipo = 'VENTA';
        }
        $orders = DB::table('orders')
            ->join('order_address', 'orders.id_orden', '=', 'order_address.id_orden')
            ->select('orders.*', 'order_address.*')
            ->where('orders.id_orden', $id_orden)
            ->where('order_address.tipo', '=', 'shipping')
            ->get();
        $products = Product::where('id_orden', $id_orden)->get();
        $prod = '';
        $xmlDiscounts = '';
        foreach ($products as $producto) {
            $prod .= '<PRODUCTO>';
            $prod .= '<CODIGO_ALFA>' . $producto->codigo_alfa . '</CODIGO_ALFA>';
            $prod .= '<REFERENCIA>' . $producto->codigo_alfa . '</REFERENCIA>';
            $prod .= '<NUMERO_TIENDA_EXTERNA>1303</NUMERO_TIENDA_EXTERNA>';
            $prod .= '<TALLA>' . $producto->talla . '</TALLA>';
            $prod .= '<IMPORTE>' . $producto->importe . '</IMPORTE>';
            $prod .= '<CANTIDAD>' . $producto->cantidad . '</CANTIDAD>';
            $prod .= '</PRODUCTO>';
            if ($producto->total_discount > 0) {
                $xmlDiscounts .='<DESCUENTO><DESCRIPCION>DESCKEDS</DESCRIPCION><IMPORTE>'.$producto->total_discount.'</IMPORTE></DESCUENTO>';
            }

        }
        foreach ($orders as $order) {
            if ($order->payment_method == 'Bank Deposit') {
                $billPaymentMethod = '21';
            } else {
                $billPaymentMethod = '01';
            }
            $xmlreq = '<?xml version="1.0" encoding="UTF-8"?>';
            $xmlreq .= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://server.hermes.ecommerce.hermes.com">';
            $xmlreq .= '<soapenv:Header/>';
            $xmlreq .= '<soapenv:Body>';
            $xmlreq .= '<ser:insertOrder>';
            $xmlreq .= '<ser:login>magentoWS</ser:login>';
            $xmlreq .= '<ser:password>magentoWS2014</ser:password>';
            $xmlreq .= '<ser:idSite>263</ser:idSite>';
            $xmlreq .= '<ser:in0>';
            $xmlreq .= '<![CDATA[<SOLICITUDES>';
            $xmlreq .= '<SOLICITUD>';
            $xmlreq .= '<TIPO>' . $tipo . '</TIPO>';
            $xmlreq .= '<NUMERO_PEDIDO_ORIGINAL>' . $order->orden_num . '</NUMERO_PEDIDO_ORIGINAL>';
            $xmlreq .= '<NUMERO_PEDIDO_ALTERNATIVO>' . $order->orden_num . '</NUMERO_PEDIDO_ALTERNATIVO>';
            $xmlreq .= '<NUMERO_TIENDA_EXTERNA>1303</NUMERO_TIENDA_EXTERNA>';
            $xmlreq .= '<FECHA_PEDIDO>' . $order->fecha_pedido . '</FECHA_PEDIDO>';
            $xmlreq .= '<TIPO_ENVIO>' . $order->shipping_method . '</TIPO_ENVIO>';
            $xmlreq .= '<ID_TIENDA>' . $order->cac_store_id . '</ID_TIENDA>';
            $xmlreq .= '<NOMBRE_ENVIO>' . $order->nombre . '</NOMBRE_ENVIO>';
            $xmlreq .= '<APELLIDO_1_ENVIO>' . $order->apellido_1 . '</APELLIDO_1_ENVIO>';
            $xmlreq .= '<APELLIDO_2_ENVIO>' . $order->apellido_2 . '</APELLIDO_2_ENVIO>';
            $xmlreq .= '<DOMICILIO_ENVIO>' . $order->calle . '</DOMICILIO_ENVIO>';
            $xmlreq .= '<NUMERO_EXTERIOR_ENVIO>' . $order->no_exterior . '</NUMERO_EXTERIOR_ENVIO>';
            $xmlreq .= '<NUMERO_INTERIOR_ENVIO>' . $order->no_interior . '</NUMERO_INTERIOR_ENVIO>';
            $xmlreq .= '<COLONIA_ENVIO>' . $order->colonia . '</COLONIA_ENVIO>';
            $xmlreq .= '<POBLACION_ENVIO>' . $order->poblacion . '</POBLACION_ENVIO>';
            $xmlreq .= '<PROVINCIA_ENVIO>' . $order->provincia . '</PROVINCIA_ENVIO>';
            $xmlreq .= '<CODIGO_POSTAL_ENVIO>' . $order->codigo_postal . '</CODIGO_POSTAL_ENVIO>';
            $xmlreq .= '<TELEFONO_ENVIO>' . $order->telefono . '</TELEFONO_ENVIO>';
            $xmlreq .= '<TELEFONO_MOVIL_ENVIO>N/A</TELEFONO_MOVIL_ENVIO>';
            $xmlreq .= '<EMAIL_ENVIO>' . $order->email_envio . '</EMAIL_ENVIO>';
            $xmlreq .= '<OBSERVACIONES>N/A</OBSERVACIONES>';
            $xmlreq .= '<IMPORTE_TOTAL>' . $order->importe_total . '</IMPORTE_TOTAL>';
            $xmlreq .= '<FORMA_PAGO>' . $billPaymentMethod . '</FORMA_PAGO>';
            $xmlreq .= '<PRODUCTOS>';
            $xmlreq .= $prod;
            $xmlreq .= '</PRODUCTOS>';
            $xmlreq .= '<DESCUENTOS>';
            $xmlreq .= $xmlDiscounts;
            $xmlreq .= '</DESCUENTOS>';
            $xmlreq .= '</SOLICITUD>';
            $xmlreq .= '</SOLICITUDES>]]>';
            $xmlreq .= '</ser:in0>';
            $xmlreq .= '</ser:insertOrder>';
            $xmlreq .= '</soapenv:Body>';
            $xmlreq .= '</soapenv:Envelope>';

        }

        $location_URL = "http://piaguimexico.com/hermesService/services/HermesImpl?wsdl";
        $client = new SoapClient(null, array(
            'location' => $location_URL,
            'uri' => $location_URL,
            'trace' => 1,
        ));

        try {
            $search_result = $client->__doRequest($xmlreq, $location_URL, $location_URL, 1);

            $dataXMLDecode = html_entity_decode($search_result);

            $pedidoOriginal = $this->get_string_between($dataXMLDecode, '<NUMERO_PEDIDO_ORIGINAL>', '</NUMERO_PEDIDO_ORIGINAL>');
            $respuestaHermes = $this->get_string_between($dataXMLDecode, '<RESPUESTA>', '</RESPUESTA>');
            $errorHermes = $this->get_string_between($dataXMLDecode, '<ERROR>', '</ERROR>');
            $pedidoHermes = $this->get_string_between($dataXMLDecode, '<PEDIDO_HERMES>', '</PEDIDO_HERMES>');
            $msjToHermes = $this->get_string_between($xmlreq, '<![CDATA[', ']]>');

            $response_xml = $client->__getLastResponse();
            // error_log("SysLog|" . date('Y-m-d H:i:s') . "|" . "CRONHER604" . "|Mensaje Hermes: {$msjToHermes}" . PHP_EOL, 3, APP_LOG_FILE);
            // error_log("Mensaje Hermes: {$msjToHermes}", 0);
            // error_log("SysLog|" . date('Y-m-d H:i:s') . "|" . "CRONHER601" . "|Respuesta Hermes: {$dataXMLDecode}" . PHP_EOL, 3, APP_LOG_FILE);
            // error_log("Respuesta Hermes: {$dataXMLDecode}", 0);
            if ($respuestaHermes == 'ERROR') {
                // error_log("SysLog|" . date('Y-m-d H:i:s') . "|" . "CRONHER602" . "|Error Hermes: {$dataXMLDecode}" . PHP_EOL, 3, APP_LOG_FILE);
                // error_log("Error Hermes: {$dataXMLDecode}", 0);
                // $apiResponse['error'] = $errorHermes;
                Log::debug('ERROR DE HERMES: ' . $dataXMLDecode);
                return response()->json(['ok' => false, 'icon' => 'warning', 'msg' => 'ERROR DE HERMES'], 200);
            }

            // $apiResponse['code'] = 200;
            // $apiResponse['message'] = array(
            //     "response" => $respuestaHermes,
            //     "pedido_original" => $pedidoOriginal,
            //     "error_hermes" => $errorHermes,
            //     "pedido_hermes" => $pedidoHermes,
            // );

            Log::debug('CANCELACION ENVIADA A HERMES CORRECTAMENTE');
            return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'Envio a HERMES correcto.'], 200);

        } catch (\SoapFault $exception) {
            //var_dump(get_class($exception));
            //var_dump($exception);
            // $apiResponse['code'] = 200;
            // $apiResponse['message'] = array(
            //     "response" => $errorHermes,
            //     "pedido_original" => $pedidoOriginal,
            //     "error_hermes" => $errorHermes,
            //     "pedido_hermes" => $pedidoHermes,
            // );
            // error_log("SysLog|" . date('Y-m-d H:i:s') . "|" . "CRONHER603" . "|Error Hermes: {$dataXMLDecode}" . PHP_EOL, 3, APP_LOG_FILE);
            // error_log("Error Hermes: {$dataXMLDecode}", 0);
            Log::debug('ERROR DE HERMES: ' . $dataXMLDecode);
            return response()->json(['ok' => false, 'icon' => 'warning', 'msg' => 'ERROR DE HERMES'], 200);
        }

    }

    public function address()
    {
        // $orders = QryOrderAddress::all();
        // return $orders;
        // $qry = DB::table('qry_product_bill_calc')
        //     ->select(DB::raw(
        //         'SUM(linea_precio_unitario*cantidad) AS monto_subtotal,
        //             SUM(linea_monto_iva) AS monto_impuestos,
        //             SUM(linea_precio_unitario*cantidad)-SUM(linea_monto_descuento) AS monto_total,
        //             SUM(linea_monto_descuento) AS monto_descuento,
        //             SUM(linea_monto_descuento) AS monto_total_descuentos,
        //             (SUM(linea_precio_unitario*cantidad)+SUM(linea_monto_iva))-SUM(linea_monto_descuento) AS monto_total_pagar, product_status'))
        //     ->where('id_orden', '=', 4202755981446)
        //     ->where('cantidad', '>', 0)
        //     ->get();
        // return $qry->count();

        // $edicom_bill = EdicomBill::all();
        // return json_decode($edicom_bill);
        // $numero = convertir(900.0);
        // return $numero;
        $qry = DB::table('qry_order_bill_calc')
            ->where('id_orden', '=', 4249551536262)
            ->get();

        dd($qry);
        Log::debug('probando tmb los logs');
    }

    public function getProducts($id_orden)
    {
        $products = Product::where('id_orden', $id_orden)->get();

        foreach ($products as $producto) {
            $items = '<PRODUCTO>';
            $items .= '<CODIGO_ALFA>' . $producto->codigo_alfa . '</CODIGO_ALFA>';
            $items .= '<REFERENCIA>' . $producto->codigo_alfa . '</REFERENCIA>';
            $items .= '<NUMERO_TIENDA_EXTERNA>1303</NUMERO_TIENDA_EXTERNA>';
            $items .= '<TALLA>' . $producto->talla . '</TALLA>';
            $items .= '<IMPORTE>' . $producto->importe . '</IMPORTE>';
            $items .= '<CANTIDAD>' . $producto->cantidad . '</CANTIDAD>';
            $items .= '</PRODUCTO>';
        }

        return $items;
    }

    public function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) {
            return '';
        }

        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public function buscar(Request $request)
    {
        $orderNum = $request->input('num');
        $order = Order::where('orden_num', $orderNum)->first();
        if (!$order) {
            return view('errors.404');
        }
        return redirect()->route('order.show', $order->id_orden);
    }

    public function transactions()
    {
        return view('orders.transactions');
    }

    public function getTransactions(Request $request)
    {
        // $orders = Transaction::query();
        $orders = DB::table('orders')
            ->select('orders.orden_num', 'orders.id_orden', 'orders.created', 'ot.json_msg')
            ->Join('order_transactions as ot', 'ot.id_orden', '=', 'orders.id_orden')
            ->get();

        // dd($orders);

        foreach ($orders as $key => $order) {
            $data = json_decode($order->json_msg);
            if (!empty($data->receipt)) {
                $receipt = json_decode($data->receipt);
            }

        }
        dd($receipt);

        // if ($request->has('params') && $request->params != 'all') {
        //     $fecha = json_decode($request->params, true);
        //     $start_date = date('Y-m-d', strtotime($fecha['date_before']));
        //     $end_date = date('Y-m-d', strtotime($fecha['date_after']));
        //     $orders->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
        // }
        // // $registros = $registers->select('*');
        // return DataTables::of($orders)
        //     // ->addColumn('full_name', function ($order) {
        //     //     return $order->nombre . ' ' . $order->apellido_2;
        //     // })
        //     ->make(true);
    }

    public function addHermesId(Request $request)
    {
        try {
            $id_orden = $request->input('id');
            $hermes_id = $request->input('opcion');

            $order = Order::findOrFail($id_orden);
            // $order->require_factura = 'si';
            $order->id_orden_hermes = $hermes_id;
            $order->save();

            return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'ID HERMES agregado correctamente.'], 200);
            // return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'NC generada correctamente'], 200);
        } catch (\Throwable $th) {
            return response()->json(['ok' => false, 'icon' => 'error', 'msg' => 'Error al añadir el ID HERMES'], 200);
        }

    }

    public function addRFC(Request $request)
    {
        try {
            $id_orden = $request->input('id');
            $rfc = $request->input('rfc');
            $code = $request->input('code');

            $order = Order::findOrFail($id_orden);
            $order->requiere_factura = 'Si';
            $order->cliente_rfc = $rfc;
            $order->cfdi_code = $code;
            $order->save();

            return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'RFC agregado correctamente.'], 200);
        } catch (\Throwable $th) {
            return response()->json(['ok' => false, 'icon' => 'error', 'msg' => $th], 200);
        }

    }

    public function sendHermesData()
    {
        $xmlreq = '';

        // try {
        // $order = Order::findOrFail(2212685807750);
        $orders = DB::table('orders')
            ->join('order_address', 'orders.id_orden', '=', 'order_address.id_orden')
            ->select('orders.*', 'order_address.*')
            ->where('order_address.tipo', '=', 'shipping')
            ->where('orders.id_orden_hermes', '=', '')
            ->where('orders.fecha_pedido', '>', '2022-05-01')
            ->where('orders.tipo_orden', '<>', 'X')
            ->get();

        $prod = '';

        foreach ($orders as $order) {
            $products = Product::where('id_orden', $order->id_orden)->get();
            foreach ($products as $producto) {
                $prod .= '<PRODUCTO>';
                $prod .= '<CODIGO_ALFA>' . $producto->codigo_alfa . '</CODIGO_ALFA>';
                $prod .= '<REFERENCIA>' . $producto->codigo_alfa . '</REFERENCIA>';
                $prod .= '<NUMERO_TIENDA_EXTERNA>1303</NUMERO_TIENDA_EXTERNA>';
                $prod .= '<TALLA>' . $producto->talla . '</TALLA>';
                $prod .= '<IMPORTE>' . $producto->importe . '</IMPORTE>';
                $prod .= '<CANTIDAD>' . $producto->cantidad . '</CANTIDAD>';
                $prod .= '</PRODUCTO>';
            }
            if ($order->payment_method == 'Bank Deposit') {
                $billPaymentMethod = '21';
                $tipo = 'RESERVA';
            } else {
                $billPaymentMethod = '01';
                $tipo = 'VENTA';
            }
            $xmlreq = '<?xml version="1.0" encoding="UTF-8"?>';
            $xmlreq .= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://server.hermes.ecommerce.hermes.com">';
            $xmlreq .= '<soapenv:Header/>';
            $xmlreq .= '<soapenv:Body>';
            $xmlreq .= '<ser:insertOrder>';
            $xmlreq .= '<ser:login>magentoWS</ser:login>';
            $xmlreq .= '<ser:password>magentoWS2014</ser:password>';
            $xmlreq .= '<ser:idSite>263</ser:idSite>';
            $xmlreq .= '<ser:in0>';
            $xmlreq .= '<![CDATA[<SOLICITUDES>';
            $xmlreq .= '<SOLICITUD>';
            $xmlreq .= '<TIPO>' . $tipo . '</TIPO>';
            $xmlreq .= '<NUMERO_PEDIDO_ORIGINAL>' . $order->orden_num . '</NUMERO_PEDIDO_ORIGINAL>';
            $xmlreq .= '<NUMERO_PEDIDO_ALTERNATIVO>' . $order->orden_num . '</NUMERO_PEDIDO_ALTERNATIVO>';
            $xmlreq .= '<NUMERO_TIENDA_EXTERNA>1303</NUMERO_TIENDA_EXTERNA>';
            $xmlreq .= '<FECHA_PEDIDO>' . $order->fecha_pedido . '</FECHA_PEDIDO>';
            $xmlreq .= '<TIPO_ENVIO>' . $order->shipping_method . '</TIPO_ENVIO>';
            $xmlreq .= '<ID_TIENDA>' . $order->cac_store_id . '</ID_TIENDA>';
            $xmlreq .= '<NOMBRE_ENVIO>' . $order->nombre . '</NOMBRE_ENVIO>';
            $xmlreq .= '<APELLIDO_1_ENVIO>' . $order->apellido_1 . '</APELLIDO_1_ENVIO>';
            $xmlreq .= '<APELLIDO_2_ENVIO>' . $order->apellido_2 . '</APELLIDO_2_ENVIO>';
            $xmlreq .= '<DOMICILIO_ENVIO>' . $order->calle . '</DOMICILIO_ENVIO>';
            $xmlreq .= '<NUMERO_EXTERIOR_ENVIO>' . $order->no_exterior . '</NUMERO_EXTERIOR_ENVIO>';
            $xmlreq .= '<NUMERO_INTERIOR_ENVIO>' . $order->no_interior . '</NUMERO_INTERIOR_ENVIO>';
            $xmlreq .= '<COLONIA_ENVIO>' . $order->colonia . '</COLONIA_ENVIO>';
            $xmlreq .= '<POBLACION_ENVIO>' . $order->poblacion . '</POBLACION_ENVIO>';
            $xmlreq .= '<PROVINCIA_ENVIO>' . $order->provincia . '</PROVINCIA_ENVIO>';
            $xmlreq .= '<CODIGO_POSTAL_ENVIO>' . $order->codigo_postal . '</CODIGO_POSTAL_ENVIO>';
            $xmlreq .= '<TELEFONO_ENVIO>' . $order->telefono . '</TELEFONO_ENVIO>';
            $xmlreq .= '<TELEFONO_MOVIL_ENVIO>N/A</TELEFONO_MOVIL_ENVIO>';
            $xmlreq .= '<EMAIL_ENVIO>' . $order->email_envio . '</EMAIL_ENVIO>';
            $xmlreq .= '<OBSERVACIONES>N/A</OBSERVACIONES>';
            $xmlreq .= '<IMPORTE_TOTAL>' . $order->importe_total . '</IMPORTE_TOTAL>';
            $xmlreq .= '<FORMA_PAGO>' . $billPaymentMethod . '</FORMA_PAGO>';
            $xmlreq .= '<PRODUCTOS>';
            $xmlreq .= $prod;
            $xmlreq .= '</PRODUCTOS>';
            $xmlreq .= '<DESCUENTOS></DESCUENTOS>';
            $xmlreq .= '</SOLICITUD>';
            $xmlreq .= '</SOLICITUDES>]]>';
            $xmlreq .= '</ser:in0>';
            $xmlreq .= '</ser:insertOrder>';
            $xmlreq .= '</soapenv:Body>';
            $xmlreq .= '</soapenv:Envelope>';

        }

        $location_URL = "http://piaguimexico.com/hermesService/services/HermesImpl?wsdl";
        $client = new SoapClient(null, array(
            'location' => $location_URL,
            'uri' => $location_URL,
            'trace' => 1,
        ));

        try {
            $search_result = $client->__doRequest($xmlreq, $location_URL, $location_URL, 1);

            $dataXMLDecode = html_entity_decode($search_result);

            $pedidoOriginal = $this->get_string_between($dataXMLDecode, '<NUMERO_PEDIDO_ORIGINAL>', '</NUMERO_PEDIDO_ORIGINAL>');
            $respuestaHermes = $this->get_string_between($dataXMLDecode, '<RESPUESTA>', '</RESPUESTA>');
            $errorHermes = $this->get_string_between($dataXMLDecode, '<ERROR>', '</ERROR>');
            $pedidoHermes = $this->get_string_between($dataXMLDecode, '<PEDIDO_HERMES>', '</PEDIDO_HERMES>');
            $msjToHermes = $this->get_string_between($xmlreq, '<![CDATA[', ']]>');

            $response_xml = $client->__getLastResponse();
            // error_log("SysLog|" . date('Y-m-d H:i:s') . "|" . "CRONHER604" . "|Mensaje Hermes: {$msjToHermes}" . PHP_EOL, 3, APP_LOG_FILE);
            // error_log("Mensaje Hermes: {$msjToHermes}", 0);
            // error_log("SysLog|" . date('Y-m-d H:i:s') . "|" . "CRONHER601" . "|Respuesta Hermes: {$dataXMLDecode}" . PHP_EOL, 3, APP_LOG_FILE);
            // error_log("Respuesta Hermes: {$dataXMLDecode}", 0);
            if ($respuestaHermes == 'ERROR') {
                // error_log("SysLog|" . date('Y-m-d H:i:s') . "|" . "CRONHER602" . "|Error Hermes: {$dataXMLDecode}" . PHP_EOL, 3, APP_LOG_FILE);
                // error_log("Error Hermes: {$dataXMLDecode}", 0);
                // $apiResponse['error'] = $errorHermes;
                Log::debug('Reenviar data a hermes');
                Log::debug('ERROR DE HERMES: ' . $dataXMLDecode);
                return response()->json(['ok' => false, 'icon' => 'warning', 'msg' => 'ERROR DE HERMES'], 200);
            }

            // $apiResponse['code'] = 200;
            // $apiResponse['message'] = array(
            //     "response" => $respuestaHermes,
            //     "pedido_original" => $pedidoOriginal,
            //     "error_hermes" => $errorHermes,
            //     "pedido_hermes" => $pedidoHermes,
            // );
            Log::debug('Reenviar data a hermes');
            Log::debug('CANCELACION ENVIADA A HERMES CORRECTAMENTE');
            return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'Envio a HERMES correcto.'], 200);

        } catch (\SoapFault $exception) {
            //var_dump(get_class($exception));
            //var_dump($exception);
            // $apiResponse['code'] = 200;
            // $apiResponse['message'] = array(
            //     "response" => $errorHermes,
            //     "pedido_original" => $pedidoOriginal,
            //     "error_hermes" => $errorHermes,
            //     "pedido_hermes" => $pedidoHermes,
            // );
            // error_log("SysLog|" . date('Y-m-d H:i:s') . "|" . "CRONHER603" . "|Error Hermes: {$dataXMLDecode}" . PHP_EOL, 3, APP_LOG_FILE);
            // error_log("Error Hermes: {$dataXMLDecode}", 0);
            Log::debug('Reenviar data a hermes');
            Log::debug('ERROR DE HERMES: ' . $dataXMLDecode);
            return response()->json(['ok' => false, 'icon' => 'warning', 'msg' => 'ERROR DE HERMES'], 200);
        }
    }

    public function resendWebHook(Request $request)
    {
        try{
            // $id_orden = $request->input('opcion');
            $id_orden = $request->num;

            // dd($id_orden);
            // $orderUrl = 'https://f7333f4b4bc33d05c05dc895f8737536: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@alpargatas-latam.myshopify.com/admin/api/2021-10/orders.json?name=' . $id_orden . '&status=any';
            // $orderUrl = 'https://'.config('app.api_key').':'.config('app.api_pass').'@'.config('app.shop_name').'.myshopify.com/admin/api/'.config('app.api_ver').'/orders.json?name=' . $id_orden;
            $orderUrl = 'https://'.config('app.shop_name').'.myshopify.com/admin/api/'.config('app.api_ver').'/orders.json?name=' . $id_orden;

            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL, 'https://'.config('app.api_key').':'.config('app.api_pass').'@'.config('app.shop_name').'.myshopify.com/admin/api/2021-10/orders.json?name=' . $id_orden);
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($ch, CURLOPT_HEADER, 0);
            // $dataOrder = curl_exec($ch);
            // curl_close($ch);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "X-Shopify-Access-Token: " . config('app.api_token')));
            curl_setopt($ch, CURLOPT_URL, $orderUrl);
            $result = curl_exec($ch);
            curl_close($ch);

            Log::debug(json_encode($result));
            $response = json_decode($result, true);
            if ($response['orders'] == '' or empty($response['orders'])) {
                return response()->json(['ok' => false, 'icon' => 'warning', 'msg' => 'La orden que estas enviando es incorrecta.']);
            }


            $innerJson = json_encode($response['orders'][0]);
            // dd($innerJson);
            Log::debug('inner');
            Log::debug($innerJson);
            $we = curl_init();
            // curl_setopt($we, CURLOPT_URL, "https://piagui.engranedigital.com/api/orders/V3/create.php");
            curl_setopt($we, CURLOPT_URL, "https://keds.engranedigital.com/api/orders/V3/create.php");
            curl_setopt($we, CURLOPT_POST, 1);
            // curl_setopt($we, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "X-Shopify-Access-Token: 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"));
            curl_setopt($we, CURLOPT_POSTFIELDS, $innerJson);
            $data = curl_exec($we);
            curl_close($we);
            Log::debug('la data de ws');
            Log::debug(json_encode($data));
            // $responseWS = json_decode($data['message']);

            // return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'Orden enviada a webservice correctamente.']);
            return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'La orden se proceso correctamente. Verifique en HERMES.']);
        } catch (\Exception $exception){
            Log::debug($exception->getMessage());
            return response()->json(['ok' => false, 'icon' => 'warning', 'msg' => $exception->getMessage()]);
        }

    }

    public function notacredito(Request $request)
    {

        Log::debug('folio:'.$request->input('folio').' y oden: '.$request->input('id'));
            $id_orden = $request->input('id');

            $opcion = $request->input('opcion');
            $folio = $request->input('folio');
            $order = Order::findOrFail($id_orden);
            // $uuid_edicom = getUuid($id_orden);
            $products = OrderProductNc::where('folio', '=', $folio)->get();
            $edicomBillUuidInvoice = EdicomBill::bySerieFa($id_orden)->pluck('uuid_factura')->first();

            Log::debug('productos: '. json_encode($products));
            if ($opcion == 'Si') {
                $order->flete = 1;
                $order->save();
            }
            if ($order->payment_method == 'Bank Deposit') {
                $billPaymentMethod = '03';
            } else {
                $billPaymentMethod = '04';
            }
            $edicomBill = new EdicomClass();
            $edicomBill->Folio = $folio;
            $edicomBill->SerieComprobante = config('app.SERIE_NC');
            $edicomBill->idOrdenShopify = $order->id_orden;
            $edicomBill->ordenNum = $order->orden_num;
            $edicomBill->RFCReceptor = $order->cliente_rfc;
            $edicomBill->ordenEmailClient = $order->email_envio;
            $edicomBill->statusProducto = 'nota_credito';
            $edicomBill->EfectoComprobante = 'E';
            $edicomBill->TipoCFD = 'NC';
            $edicomBill->FormaPago = $billPaymentMethod;
            $edicomBill->prodctosToNC = $products;
            if ($opcion == 'Si') {
                $edicomBill->envioEstandarPrecioConIva = floatval($order->shipping_price);
            } else {
                $edicomBill->envioEstandarPrecioConIva = 0;
            }
            $edicomBill->UUID = $edicomBillUuidInvoice;
            $edicomBill->uudRelacional = $edicomBillUuidInvoice;
            $edicomBill->createBill();
            Log::debug('Generacion de nota de credito exitosa');
            return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'NC generada correctamente'], 200);
    }

    public function creditNote(Request $request)
    {
        $productos = $request->input('products');

        // $envio = $request->input('envio');
        $orderId = $request->input('id');

        // Log::debug($orderId);
        $order = Order::findOrFail($orderId);
        $dateNow = date('Y-m-d H:i:s');

        $order = EdicomBill::create(
            [
                'serie' => config('app.SERIE_NC'),
                'id_orden' => $order->id_orden,
                'orden_num' => $order->orden_num,
                'bill_status' => 'generated',
                'rfc' => $order->cliente_rfc,
                'email' => $order->email_envio,
                'created' => $dateNow
            ]
        );
        $folio = $order->folio;
        // $folio = 384;

        foreach ($productos['dataToDelete'] as $key => $value) {
            $productsNC[$key]['id'] =  $value[1];
            $product = Product::where('line_item_id', '=', $value[1])->first();
            $save = $this->savedProductsNc($product, $folio);
        }

        // $products = $this->savedProductsNc($productsNC, $folio);

        $return_url = route('order.show_nc', ['orderId'=>$order->id_orden, 'folio'=> $folio]);
        // Log::debug('/order/'.$order->id_orden.'/nota-credito/'.$folio);
        return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'NC generada correctamente', 'return_url'=> $return_url], 200);
        // // return route('orders.notacredito', $orderId, $folio);
        // // return view('orders.nota_credito', ['order'=>$order, 'products'=>$products])

    }

    public function showCreditNote(Request $request, $orderId, $folio)
    {
        // dd($folio);
        $order = Order::findOrFail($orderId);
        $edicomBill = EdicomBill::where('serie', '=', config('app.SERIE_NC'))->where('folio', '=', $folio)->first();
        $products = OrderProductNc::where('folio', '=', $folio)->get();

        return view('orders.nota_credito', ['order'=>$order, 'notaNc'=> $edicomBill, 'products'=>$products]);
    }

    function savedProductsNc($product, $folio)
    {
        Log::debug($product);
        try {
            $folio_nc = $folio;
            $nc = OrderProductNc::updateOrCreate(
                [
                    'folio' => $folio_nc,
                    'line_item_id' => $product->line_item_id,
                    'id_orden' => $product->id_orden,
                ],
                [
                'folio' => $folio_nc,
                'line_item_id' => $product->line_item_id,
                'id_orden' => $product->id_orden,
                'codigo_alfa' => $product->codigo_alfa,
                'sku' => $product->sku,
                'cantidad' => 0,
                'importe' => $product->importe,
                'referencia' => $product->referencia,
                'talla' => $product->talla,
                'title' => $product->title,
                'total_discount' => $product->total_discount,
                'variant_title' => $product->variant_title,
                'precio_base'=> $product->precio_base,
                'descuento_base'=> $product->descuento_base
            ]);
        } catch (\Exception $th) {
            Log::debug($th->getMessage());
        }
    }

    public function updateDateOrder(Request $request)
    {
        try {
            // $id = 4854572286086;
            $id_orden = $request->input('id');
            $date_order = $request->input('date');

            $order = Order::findOrFail($id_orden);
            $fecha = date('Y-m-d',strtotime($order->fecha_pedido));
            $hora = date('H:i:s',strtotime($order->fecha_pedido));
            $date = Carbon::parse($date_order.' '.$hora);
            $order->fecha_pedido = $date;
            $order->save();

            return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'Fecha de pedido actualizada'], 200);
        } catch (\Throwable $th) {
            return response()->json(['ok' => false, 'icon' => 'error', 'msg' => $th], 200);
        }


    }

    public function cancelOrderEdicom(Request $request)
    {
        $order_id = $request->input('order_id');
        $invoice_folio = $request->input('invoice_folio');
        // $order_id = 4952595103805;
        Log::debug('id de la orden '.$order_id);
        // $motive = $request->input('motive');
        // $sustitute = $request->input('sustitute');

        $serie = config('app.SERIE_FACTURA');
        // dd($serie);
        $order = EdicomBill::where('id_orden', $order_id)->where('serie', '=', $serie)->where('folio', '=', $invoice_folio)->first();
        // dd($order);

        $document = getDocumentB64($order->verification_code);
        Log::debug('documento');
        Log::debug($document);
        if(isset($document['error'])){
            return response()->json(['ok'=>false, 'icon'=>'warning', 'msg'=>$document['message']]);
        }
        // $uuid = $this->getUUID($document['documentB64']);
        $dataXML = base64_decode($document['documentB64']);
        $array = XmlToArray::convert($dataXML);
        // dd($array);
        // dd($array['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['UUID']);
        // echo var_dump(array(
        //     $array['cfdi:Emisor']['@attributes']['Rfc'],
        //     $array['cfdi:Receptor']['@attributes']['Rfc'],
        //     $array['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['UUID'],
        //     $array['@attributes']['Certificado'],
        //     $array['@attributes']['Total'],
        // ));
            $data = array(
                    $array['cfdi:Emisor']['@attributes']['Rfc'],
                    $array['cfdi:Receptor']['@attributes']['Rfc'],
                    $array['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['UUID'],
                    $array['@attributes']['Certificado'],
                    $array['@attributes']['Total'],
            );
            Log::debug(json_encode($data));
        $cancelEdicom = $this->sendCancelToEdicom($array);


        if (isset($cancelEdicom['error'])) {
            return response()->json(['ok'=>false, 'icon'=>'warning', 'msg'=>$cancelEdicom['message']]);
        }

        $order->delete();

        return response()->json(['ok' => true, 'icon' => 'success', 'msg' => $cancelEdicom['message']]);

    }



    private function sendCancelToEdicom($data)
    {
        $xmlreq = '<?xml version="1.0" encoding="UTF-8"?>';
        $xmlreq .= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cfdi="http://cfdi.service.ediwinws.edicom.com">';
        $xmlreq .= '<soapenv:Header/>';
        $xmlreq .= '<soapenv:Body>';
        $xmlreq .= '<cfdi:cancelCFDiAsync>';
        $xmlreq .= '<cfdi:user>LHB1508052E7</cfdi:user>';
        $xmlreq .= '<cfdi:password>p13fnnqgtg</cfdi:password>';
        $xmlreq .= '<cfdi:rfcE>'.$data['cfdi:Emisor']['@attributes']['Rfc'].'</cfdi:rfcE>';
        $xmlreq .= '<cfdi:rfcR>'.$data['cfdi:Receptor']['@attributes']['Rfc'].'</cfdi:rfcR>';
        $xmlreq .= '<cfdi:uuid>'.$data['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['UUID'].'</cfdi:uuid>';
        $xmlreq .= '<cfdi:pfx>MIINFQIBAzCCDM4GCSqGSIb3DQEHAaCCDL8Eggy7MIIMtzCCBWMGCSqGSIb3DQEHAaCCBVQEggVQMIIFTDCCBUgGCyqGSIb3DQEMCgECoIIE+zCCBPcwKQYKKoZIhvcNAQwBAzAbBBTC4eDiWxQmS1IkYujXC6H/29achwIDAMNQBIIEyEQoSeU40J6adJ4MGXhEJ2msLbzxBFCcDG4xxUhzZfCHmyjQqhqsfXpELrZAvg/LGmuxzSXD6czhQ06FbVHLZirRV0GLrMcIzIyrqMYnfydk3N0x19H2tEREyMfGnpOFA7fHmemVzHgXa+9u9QIq/fPYfNC2FJXQiHD073UCenmW0LJyMJtmZE9eT/tgkO4nDVa+ODo/+p687D1VaIYUud9RmyZVb4+tsKYRSBQF7z5Vk7rCNMPiDAR/YyVMcy/kW5XtDNNVU5XJE0hhkYCLhR4M3rxZyaqeQNYuo7NQc1Oi/ULwGbvIr/0brfG+rUcPgTrHk6BTl6XdmfMwDDPuGSR2FV1oTFgxwFnoZHyTsh0teh73MQGygYCt8sc48cuF3ZL15rdCxzkqOt/o5Ec0dsBodY7U72yqJdCqN1uRgE/amUlAJyQZgwjxJIlZGUxrmdflhs480AEfzkTUlWV+yivpVgDd57XJcbr0Xxqm1Ak4edUn0hTR/FWs4lmfqRekNja1Yq1eZtj8g1mmw4Rp6zKlps0L705wCQxgww7elg0O35H/kdCC3V52ZliauN5UyOIV8DKLWrmqlww19uV+UGmzikw4fw/+82CJFvZblbz5faHK5QlH8BNH4C0yoWJVT1NVmM/N9CW4rhkFGzFV8ennbBSGL2v4kSE8HpJzB2ytxOMUdqSjhU1fVMoNOt5OtS2NNeyiHY0MSay7ANGiquuQcHfwhQ+XEwEdWhR8QOFc/AlULFuNtmubqk1Lt151NeVJZaTbUFhLvzRXt3dtnb3nmWDq1bCW76gowtNNCQE2W+40/qThob1oSsvBtz+FXgOWwWhATeoBcLKh+Z9QAD6FF8nrHMuaXw1CA292l2nkL7NhN41g4c2mfoD/HOeiHLptS499d7luemz1+cDOidKRW8AfqMc9hlSJeJ6gJaSowLqcOjxDWZ+TZMwWedf8ChzgfqtWRMFze/vwESQRomD+nOt0D0GgyqOzeaqEt1fua1OvDWCSM6+e21ZEnO/5PS20LdCUtf7Jfwa16Eke4CB6iQf29dWRtVHf7eb4G4wVie6nKqE21KHe6oy6qytXo2uZCxd0SRQQSNdXkP+IhivT/wT9KIMb2HAt0cwH3y3gCbVmJeNPPmWt9oy/DGthHDo+cfMDrNb8SVHLRp8WtQPE1aDdlIpQRPzKTYl9f2hBey7L9Cxm12r2BPVlnkFiHUbScgQcpvWNfXhiTMSV/lkgoabj+K4WmY+gDUYjTJd4YFtO4b3ZP7VMhOaQtXSauVGOja1jnuqWVfKwiGfYGAtOumraSlNEy11Xt1FXCYEQuANELHEAU8Ob1BBqN1mfvvsK93vhv68mJNmxpfCS2s8M3FsmZIVj6u89RkzcKBGSeHGPtCibxDL67eB0isjNH17tAyTduIc+sewag/m8+grthb2UcbFGnOeTVejuIcayLJ2YNvnY0k/AtnRolU8W+AmwAY+gogKu5Y3/PUFeXi0a8xEaqkDeEIaBZQ7IQN/y7CNeyPBG/e2DMlpteO0o0Lz3TX3xDlNbto2ImrSKVfoE6APmwlaFV6WKaxQeenyncSZVDJ5D0MyjBWDbfvmU+1hTWRspt4VF+hYkcy14Py6fRqiefG0SlzE6MBUGCSqGSIb3DQEJFDEIHgYAawBlAHkwIQYJKoZIhvcNAQkVMRQEElRpbWUgMTcwOTc0NjA4MDI2MDCCB0wGCSqGSIb3DQEHBqCCBz0wggc5AgEAMIIHMgYJKoZIhvcNAQcBMCkGCiqGSIb3DQEMAQYwGwQUv0EVIsE9bf3+ichBPOmPvcjyblUCAwDDUICCBvhqSzK6MXYLz4u42yv84wmjuE9Yl3De7FM6tbhxTIfPr9QhGAMFTdWKX9GW8Y+XKtf29IfAwfi/qXB+8ecuHLq4hecNJJob7J9BPHeEjdPiXjmnOU2784MyOuFRAW4nhAdzcn89xrFnomfEEHbBFlmxHBFFqmtcuj5XnMwVvA5/a/XSrqxYVv4TErTZeY1t9bGzKIDD1p/fIsA8ItrIBmeF4pRKe4OhjtzyDEH6IOdVe7ZglO+aOP9h+uYrbEDVYyLGVbOD41+tyu0XrObTYvd2XZY/JKecEXPu3NhBhdfqqnC8H8fk3sq96C9tDdKh8Eh0HMt5nG3mjWZY58ibJrH9yEcAUEjS4ohK/opPgrMg6kvrZwfymzF+BztKFfHcd3cN/1/r95mkv4j/EJhzeuYRvWfjh33pxXsv7WGUcxpSA/MCirHh0bB/Ckd5w46mce2V4ztlrq75O6IFZTEBp9BS9437SyQjzXvE1/zL2nVzgSn0mgr+CPbARwUzpm4mVJ/JfytStfaMWvhY+Pn21JBEWDtzjBmBOaeYAMP1GZmX0L2LJUppNo9FK9EC2rdvQ67HEnvLk5jciWoxWy0SXP4iP7bjf5sGj/MEWovEl9NvXdXE/1LIrLhFJpOReXws+yovIJTf7Vj+8+EX5Xn5i8FgjXDHaLt/m8bCAsLqnwEmQxE00YRxxAumm8xq5jHA5EVEz0hxiT5OKv/kII2/qDILvHJu83UcYUK1dJUPpauE1RvADLBSyAZq2IwHKEjFdHsCNufXFYnl6xnC2p2PDRmxfWpTQWbU1w5e3Bebi2bvDdvGYc6ww7yyBwNpQYDJssMZmja7ooA6uwskotApBwR+dt4AAlUqJEV8/2duO0rGW8PTEuguEfSPpg0mdWn0C+tIUrnXvtBkXWTy/buhH/+VTwmXj/pNXqaPFf9T3amjm8DYMazRclS9nOv+aYXNJwqP7fAc6jyUmvbdpvPIKna6xyrO9/Vx5F/bN8A0d+FV5vataXjnUjglVnAt94jjmBlTAMcI7W7v/bcyK52+dLKcJKfIWvdh9Vn6MIgt5CfJn46oPLL+Oh85RMVsuyt9A5U/b97RaMb8tBXY1Caa2wAAWiVa8m0UfGtIZnAlP44WCMo6e0tq4reIKThMHSbfQUwgT89jFTVmwLobppeSZvGhquT9nne7LgQvnJMGopbd+V13yNBoMZGNDc5JbZFUtZFTwZpIT7oOkI7ylo+zjPEFbi8/emuSYhv7zc8fEwFSgfzv9qowB7mz060lVL2quZb44G/BfcbIWZmSzKLdPLi6gIUvbjsxz++pXLuevfj5D0lB5kedxs3pnPNaxxhX7F6EMtXH1SajmrEyi9k9H1RXFabrozul7pDeJOJIHjFUm++KZUDRGNa+gzbQtqh5kO4AR3e2uy/C7RTUWMVG0vsceoX3IfCwIcn9629svm2jrx1G6uk3GCp+6SchXKY8dq4ddk+dvmARcIUB/MSDADUr7g4fBFhq/E3uIbN690v6C6QPkIM6Ln34ps/j5TEwye5WzTkVTBxZZSyfwyUnrzH9nDHWfA2yjt/D9hiu3hLr3Tuy0b41NEB7JlA9XEuXRk5hcboHx+Q0gLSTvdrdE4rKCgGfNqRNTKg+WDN/KRYivBgugLu4JvpeN7LR2ALhoNiXJrr5S1N08l916j7oxOF73rbdqcQnLsv7jYoN1IK864PdGKiXn2nW9p5Qo6kreUn73HVteNDhc//55d0TLE7ZzzeiKKaGCXb2e3RAGiCCl6wFhCmGgJUgfPd3LhkqedYQzUvHipTkz7+rtHZS2d0S4VCjg2JVIBdhlFwdvzGITUdLTo3jkw8hRax+2AgqmG7GxmZvBRKL+Ivfz+lUEp8Ed6ZbLWGAaujaVcAYfGCfaoqNXsdJyVgGHnYORInoiYNSwBzGt3/vlXyvxJ7VlWOSRAwGBzBoKwIWDBWbyyyD963e0Y/a8JSFCl+JvwB8rl4iq2byhLeiQ8SFwrwq6mn4dWrAcTFRbgObOhFfEqWO5LsPJr6e+B/p4fs+2Xz8AMPc7qSV9Ek+sOjc48gTkvZsOtDWlPyNZU9yw/gMcUea+lxHoemIDGcxSLzHQP5AhixEOdJJGSBUjXx0MElbCytGVBahujM0Fb8QSy4KuX3m9INUa2mAtxwrrwKGgMM4h9nMVOHOtaecRnpJbt0ppOBJITXm5MbiTnk8RnumErmn0LLlxAjKStxbVqXRjIcLSR7/6oF7JTZDkuuw2IPZybsrAfUU7wGIx2EE/9M3aJEQqZOca6n3L52/Ctxyzbs20dAOGVFT52vSlyBt9jrcg85vGE1VJZHJsespKG8xywS3tMLpg4DL3JTmWYBhPgCP2LvYr0qW+ANPXTA+MCEwCQYFKw4DAhoFAAQU2YgwYesURiGlif5k8qJMUNG2EmIEFFtpZc5NOx7ecXB8AsiamvQhlWHGAgMBhqA=</cfdi:pfx>';
        $xmlreq .= '<cfdi:total>'.$data['@attributes']['Total'].'</cfdi:total>';
        $xmlreq .= '<cfdi:pfxPassword>'.config('app.ediwin_passpfx').'</cfdi:pfxPassword>';
        $xmlreq .= '<cfdi:motivo>03</cfdi:motivo>';
        $xmlreq .= '<cfdi:test>false</cfdi:test>';
        $xmlreq .= '</cfdi:cancelCFDiAsync>';
        $xmlreq .= '</soapenv:Body>';
        $xmlreq .= '</soapenv:Envelope>';

            Log::debug('llamada a edicom xml');
            Log::debug($xmlreq);

            $location_URL = "https://cfdiws.sedeb2b.com/EdiwinWS/services/CFDi?wsdl";
            $client = new SoapClient(null, array(
                'location' => $location_URL,
                'uri' => $location_URL,
                'trace' => 1,
            ));

        try {
            $search_result = $client->__doRequest($xmlreq, $location_URL, $location_URL, 1);

            /* Convertir el mensaje a texto decodificando el mensaje */
            $dataXMLDecode = html_entity_decode($search_result);
            Log::debug('respuesta de edicom');
            Log::debug($dataXMLDecode);
            /* -------Parse de respuesta hermes---------- */
            // $documentReturn = get_string_between($dataXMLDecode, '<getDocumentReturn>', '</getDocumentReturn>');
            $documentReturn = get_string_between($dataXMLDecode, '<isCancelable>', '</isCancelable>');
            $errorcode = get_string_between($dataXMLDecode, '<ns1:cod>', '</ns1:cod>');
            $errormeesaje = get_string_between($dataXMLDecode, '<ns1:text>', '</ns1:text>');
            $response_xml = $client->__getLastResponse();
            /* TODO: corregir la respuesta de hermes  */
            if ($errorcode) {
                $response['code'] = $errorcode;
                $response['error'] = true;
                $response['message'] = $errormeesaje;
            }

            $response['code'] = 200;
            $response['message'] = $documentReturn;

            return $response;

        } catch (\SoapFault $exception) {
            //var_dump(get_class($exception));
            //var_dump($exception);
            return response()->json(['code'=>400, 'error'=>$errorcode, 'message'=>$exception->getMessage()]);

        }

    }

}
