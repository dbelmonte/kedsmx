<?php

namespace App\Http\Controllers;

use Exception;
use ZipArchive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Artisaninweb\SoapWrapper\SoapWrapper;

class SoapController extends Controller
{
    /**
   * @var SoapWrapper
   */
  protected $soapWrapper;

  /**
   * SoapController constructor.
   *
   * @param SoapWrapper $soapWrapper
   */
  public function __construct(SoapWrapper $soapWrapper)
  {
    $this->soapWrapper = $soapWrapper;
  }


    public function download(Request $request)
    {
        $folio = $request->folio;
        Log::debug($request->folio);

        $user = config('app.ediwin_usr');
        $password = config('app.ediwin_passwd');
        $domain = config('app.ediwin_domain');
        $group = "0";
        $clientType = "1";


        $this->soapWrapper->add('Service', function ($service) {
            $service
                ->wsdl('https://ediwinws.sedeb2b.com/EdiwinWS/services/EdiwinWS?wsdl')
                ->trace(true);
        });


        // // Without classmap
        $response = $this->soapWrapper->call('Service.registerSession', [
            'user' => $user,
            'password' => $password,
            'domain' => $domain,
            /*'group' => $group,*/
            'clientType' => $clientType,
        ]);
        // Log::debug(json_encode($response));
//'filter' => "REFERENCIA='AAZ8192' AND SITUACION='RCP' AND ENV_ORIGEN ='WME921015US1'",
//'filter' => "REFERENCIA='AAZ8192' AND SITUACION='RCP' AND ENV_ORIGEN ='WME921015US1'",
        
        $referencia = "REFERENCIA='".$folio."' AND DESTINO='BUSINESSMAIL'";
        Log::debug($referencia);
        // funciona, falta cambiar el nombre del fichero
        $response = $this->soapWrapper->call('Service.getDocumentsB64', [
            'filter' => $referencia,
            'type'   => '4',
            'documentTypeList' => '',
            'idVolume' => '0',
        ]); 

            /**
             * si uso esta y el valor 4 me regresa un error de mapeo
             */
        // $response = $this->soapWrapper->call('Service.exportDocumentsB64', [
        //     'filter' => "REFERENCIA='AMASS3122'",
        //     'type'   => '4',
        //     'documentTypeList' => '',
        //     'idVolume' => '0',
        //     'fileName' => 'AMASS3122-test',
        //     'showDate' => true

        // ]); 
        
        // $response = $this->soapWrapper->call('Service.exportDocuments', [
        //     'filter' => "REFERENCIA='AMASS3122'",
        //     'type'   => '4',
        //     'process' => 'MAPA_INTEGRACION_CFDI_TPV_WMX',
        //     'documentTypeList' => '',
        //     'idVolume' => '0',
        //     'fileName' => 'AMASS3122-test',
        //     'showDate' => true

        // ]);
        Log::debug(json_encode($response));

        // dd($response);
        $file = base64_encode($response);
        // dd($file);
        $public_dir = public_path();
        $attachment = $folio.'.zip';
        $path       = $public_dir . '/storage/' .$attachment;
        $contents   = base64_decode($file);
        
        //store file temporarily
        file_put_contents($path, $contents);

        $zip = new ZipArchive;
        $res = $zip->open($path);
        if ($res === TRUE) {
            $zip->renameName('composer.pdf',$folio.'.pdf');
            $zip->close();
        } else {
            echo 'failed, code:' . $res;
        }
        
        //download file and delete it
        return response()->download($path)->deleteFileAfterSend(true);

    }
}
