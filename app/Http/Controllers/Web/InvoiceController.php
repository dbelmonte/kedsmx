<?php

namespace App\Http\Controllers\Web;

use App\EdicomBill;
use App\Http\Controllers\Controller;
use App\Jobs\DeleteFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use SoapClient;
use ZipArchive;

class InvoiceController extends Controller
{

    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order' => 'required|string',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return response()->json('Hubo un error al generar la factura. Cominicate con atención al cliente.');
        }

        // return response()->json('hola');
        $user = config('app.ediwin_usr');
        $password = config('app.ediwin_passwd');
        $domain = config('app.ediwin_domain');
        $clientType = "1";

        $order = $request->input('order');
        $email = $request->input('email');
        $bill = EdicomBill::where('orden_num', '=', $order)->where('email', '=', $email)->where('serie', '=', config('app.SERIE_FACTURA'))->where('bill_status', '=', 'confirmed')->first();

        if ($bill == null) {
            return response()->json(['msg'=>'Hubo un error al generar la factura. Puede que no este confirmada/timbrada o tenga un error. Cominicate con atención al cliente.', 'error'=>true]);
        }
        $now = Carbon::now();
        $date = Carbon::parse($bill->created);
        $diff = $date->diffInMonths($now);

        if (empty($bill->rfc)) {
            return response()->json(['msg'=>'Este pedido no requiere factura. Si requiere factura de este pedido, comuniquese con Atención al Cliente.', 'error'=>true]);
        }

        if ($diff >= 3) {
            return response()->json(['msg'=>'Se ha sobrepasado el tiempo permitido para poder descargar tu factura. Cominicate con atención al cliente.', 'error'=>true]);
        }

        $folio = $bill->serie.$bill->folio;

        $client = new SoapClient("https://ediwinws.sedeb2b.com/EdiwinWS/services/EdiwinWS?wsdl");

        try {
            $response = $client->__soapCall('registerSession', array(
                'user' => $user,
                'password' => $password,
                'domain' => $domain,
                'clientType' => $clientType,
            ));
            $referencia = "REFERENCIA='".$folio."'";
            // $referencia = "REFERENCIA='AML221576'";
            $response = $client->__soapCall('getDocumentsB64', array(
                'filter' => $referencia,
                'type'   => '4',
                'documentTypeList' => '',
                'idVolume' => '',
            ));

            $file = base64_encode($response);
            $public_dir = public_path();
            $attachment = $folio.'.zip';
            // $path       = $public_dir . '/storage/fac/' .$attachment;
            $path = '/var/www/html/facturacion/storage/'.$attachment;
            $contents   = base64_decode($file);

            //store file temporarily
            file_put_contents($path, $contents);

            $zip = new ZipArchive;
            $res = $zip->open($path);
            if ($res === TRUE) {
                $zip->renameName('composer.pdf',$folio.'.pdf');
                $zip->close();
            } else {
                echo 'failed, code:' . $res;
            }

            //download file and delete it
            // return response()->download($path)->deleteFileAfterSend(true);

            return response()->json(['msg'=>'', 'url' => '/storage/'.$attachment, 'error'=>false]);
            DeleteFile::dispatch($attachment)->delay(now()->addMinutes(1));
        } catch (\SoapFault $th) {
            return response()->json(['msg'=>'Hubo un error al generar la factura. Cominicate con atención al cliente.', 'error'=>true]);
        }

    }

}
