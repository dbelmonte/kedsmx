<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class AutomationController extends Controller
{
    public function getAutomation(Request $request)
    {

        $dataOrigin = file_get_contents('php://input');
        Log::debug('Data de Shopify');
        Log::debug($dataOrigin);

        $data = json_decode($dataOrigin);
        $jsonStr = json_encode($data, true);
        Log::debug('Data de json');
        Log::debug($jsonStr);
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://mcg-th5bjtggsl458w-yx4tvssbq.auth.marketingcloudapis.com/v2/token',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_MAXREDIRS => 10, //default is 20
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => http_build_query(array(
        "grant_type"=> 'client_credentials',
        "client_id"=> 'w3q9euel88kk5d2yrwbr25lz',
        "client_secret"=> 'J6B4lpLHGfU2FzAzPExvuiyd',
        "account_id"=> '534006168',
        "format"=>"json"
        )),
        CURLOPT_HTTPHEADER => array('Content-Type: application/x-www-form-urlencoded'),
        ));
        $response_j = curl_exec($curl);
        curl_close($curl);
        Log::debug($response_j);
        $response_o = json_decode($response_j);


        // $params = json_encode($jsonStr, JSON_PRETTY_PRINT);
        $url = 'https://mcg-th5bjtggsl458w-yx4tvssbq.rest.marketingcloudapis.com/interaction/v1/events';
        $curl = curl_init( $url );
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonStr);
        $authorization = "Authorization: Bearer ".$response_o->access_token; // Prepare the authorisation token
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json", $authorization));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        $json_response = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($json_response,true);
        Log::debug($json_response);
        return response($json_response);


    }
}
