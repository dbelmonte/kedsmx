<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function getDiscount(Request $request, $product)
    {
        $id_product = $product;
        // $payload = $this->getPayloadForFulfillment($id_product);
        $api_endpoint = 'products/'.$id_product.'.json';
        
        $endpoint = getShopifyURLForStore($api_endpoint); 
        $headers = getShopifyHeadersForStore();
        
        $response = makeAnAPICallToShopify('GET', $endpoint, $headers, null); 
        
        $product = $response['body']['product']['variants'][0];
       

        $discount = 0;
        $compare_price = $product['price'];
        if ($product['compare_at_price'] > $product['price']) {
            $discount = $product['compare_at_price'] - $product['price'];
            $compare_price = $product['compare_at_price'];
        }
        return response()->json([
            'descuento' => floatval($discount),
            'precio_base' => floatval($compare_price),
        ]);
    }
}
