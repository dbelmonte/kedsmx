<?php

namespace App\Http\Controllers\API;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Controllers\EdicomClass;

class OrdersController extends Controller
{
    public function addUidToInvoice(Request $request)
    {
       

        try {
            $data = file_get_contents('php://input');
            $dataArray = json_decode($data);

            if ($data == null) {
                return response()->json(['code'=> 'error', 'msg' => 'Datos vacios o el valor es incorrecto']);
            }

            $uuid_edicom = getUuid($dataArray->code);

            $updateEdicomBillUuid = DB::table('edicom_bills')
            ->where('serie', '=', $dataArray->serie)
            ->where('folio', '=', $dataArray->folio)
            ->update([
                'error_msg' => '',
                'uuid_factura' => $uuid_edicom
            ]);

            return response()->json(['code'=> 'success', 'msg' => $uuid_edicom ]);
        } catch (\Throwable $th) {
            return response()->json(['code'=> 'error', 'msg' => 'Se produjo un error al generar u obtener el uuid']);
        }

     
    }

    public function createOrder(Request $request)
    {
        try {
            $dataItems = file_get_contents('php://input');
            $data = json_decode($dataItems, true);

            $order = Order::where('id_orden','=', $data['id_orden'])->first();
            if ($order->payment_method == 'Bank Deposit') {
                $billPaymentMethod = '03';
            } else {
                $billPaymentMethod = '04';
            }
            $edicomBill = new EdicomClass();
            $edicomBill->SerieComprobante = 'ABK';
            $edicomBill->idOrdenShopify = $order->id_orden;
            $edicomBill->ordenNum = $order->orden_num;
            $edicomBill->RFCReceptor = $order->cliente_rfc;
            $edicomBill->ordenEmailClient = $order->email_envio;
            $edicomBill->statusProducto = 'preparado';
            $edicomBill->EfectoComprobante = 'I';
            $edicomBill->FormaPago = $billPaymentMethod;
            $edicomBill->envioEstandarPrecioConIva = floatval($order->shipping_price);
            if(!empty($order->uso_cfdi)){
                $edicomBill->Misc32 = $order->uso_cfdi;
            }
            $edicomBill->createBill();

            Log::debug('Generacion de factura exitosa');
            return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'Factura generada correctamente'], 200);
        } catch (\Throwable $th) {
            Log::debug($th);
            return response()->json(['ok' => false, 'icon' => 'error', 'msg' => 'Error al general la factura'], 200);
        }
    }

}
