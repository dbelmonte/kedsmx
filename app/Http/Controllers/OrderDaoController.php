<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderProductNc;
use App\QryOrderAddress;
use App\QryOrderBillCalc;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderDaoController extends Controller
{
    //--Orders--
    public $idOrdenShopify = 0;
    public $tipoOrden = '';
    public $orderNum = '';
    public $updatedAt = '';
    public $fechaPedido = '';
    public $nombre = '';
    public $apellido1 = '';
    public $apellido2 = '';
    public $emailEnvio = '';
    public $importeTotal = 0;
    public $paymentStatus = '';
    public $idOrdenHermes = '';
    public $requiereFactura = '';
    public $clienteRfc = '';
    public $shippingMethod = '';
    public $cacStoreId = '';
    public $orderStatus = '';
    public $hermesHerror = '';
    public $totalDiscounts = 0;
    public $shippingPrice = 0;
    public $orderMontoSubtotal = 0;
    public $orderMontoImpuestos = 0;
    public $orderMontoTotal = 0;
    public $orderMontoDescuento = 0;
    public $orderMontoTotalDescuentos = 0;
    public $orderMontoTotalPagar = 0;

    //--Products--
    public $lineItemId = 0;
    public $idOrden = 0;
    public $codigoAlfa = '';
    public $sku = '';
    public $cantidad = 0;
    public $importe = 0;
    public $productStatus = '';
    public $referencia = '';
    public $talla = '';
    public $title = '';
    public $totalDiscount = 0;
    public $varianTitle = '';
    public $totalProductsInOrder = 0;
    public $productsData = [];

    //--Address--
    public $calle = '';
    public $noExterior = '';
    public $noInterior = '';
    public $colonia = '';
    public $poblacion = '';
    public $provincia = '';
    public $pais = '';
    public $codigoPostal = '';
    public $telefono = '';

    public $responseError = '';

    public $cfdi_code = '';

    
    public $productsToNC = [];


    public function __construct()
    {

    }

    // Obtener informacion de la orden por el ID Shopify
    public function getOrderDataById()
    {
        $order = Order::findOrfail($this->idOrdenShopify);
        if ($order->count() <= 0) {
            $this->responseError = 'No existen datos de la orden con el ID:' . $this->idOrdenShopify;
            return false;
        }

        $this->idOrdenShopify = $order->id_orden;
        $this->tipoOrden = $order->tipo_orden;
        $this->orderNum = $order->orden_num;
        $this->updatedAt = $order->updated_at;
        $this->fechaPedido = $order->fecha_pedido;
        $this->nombre = $order->nombre;
        $this->apellido1 = $order->apellido_1;
        $this->apellido2 = $order->apellido_2;
        $this->emailEnvio = $order->email_envio;
        $this->importeTotal = $order->importe_total;
        $this->paymentStatus = $order->payment_status;
        $this->idOrdenHermes = $order->id_orden_hermes;
        $this->requiereFactura = $order->requiere_factura;
        $this->clienteRfc = $order->cliente_rfc;
        $this->shippingMethod = $order->shipping_method;
        $this->cacStoreId = $order->cac_store_id;
        $this->orderStatus = $order->order_status;
        $this->hermesHerror = $order->hermes_error;
        $this->totalDiscounts = $order->total_discounts;
        $this->cfdi_code = $order->cfdi_code;
        return true;

    }

    // Obtener informacion de la orden por el ID Shopify con datos de facuracion
    public function getOrderBillingDataById()
    {
        $qryOrderAddress = QryOrderAddress::where('id_orden', '=', $this->idOrdenShopify)->where('direccion_tipo', '=', 'billing')->first();

        if ($qryOrderAddress->count() > 0) {

            $this->idOrdenShopify = $qryOrderAddress->id_orden;
            $this->tipoOrden = $qryOrderAddress->tipo_orden;
            $this->orderNum = $qryOrderAddress->orden_num;
            $this->nombre = $qryOrderAddress->nombre;
            $this->apellido1 = $qryOrderAddress->apellido_1;
            $this->apellido2 = $qryOrderAddress->apellido_2;
            $this->emailEnvio = $qryOrderAddress->email_envio;
            $this->importeTotal = $qryOrderAddress->importe_total;
            $this->paymentStatus = $qryOrderAddress->payment_status;
            $this->idOrdenHermes = $qryOrderAddress->id_orden_hermes;
            $this->requiereFactura = $qryOrderAddress->requiere_factura;
            $this->clienteRfc = $qryOrderAddress->cliente_rfc;
            $this->shippingMethod = $qryOrderAddress->shipping_method;
            $this->cacStoreId = $qryOrderAddress->cac_store_id;
            $this->orderStatus = $qryOrderAddress->order_status;
            $this->totalDiscounts = $qryOrderAddress->total_discounts;
            $this->calle = $qryOrderAddress->calle;
            $this->noExterior = $qryOrderAddress->no_exterior;
            $this->noInterior = $qryOrderAddress->no_interior;
            $this->colonia = $qryOrderAddress->colonia;
            $this->poblacion = $qryOrderAddress->poblacion;
            $this->provincia = $qryOrderAddress->provincia;
            $this->pais = $qryOrderAddress->pais;
            $this->codigoPostal = $qryOrderAddress->codigo_postal;
            $this->telefono = $qryOrderAddress->telefono;
            $this->fechaPedido = $qryOrderAddress->fecha_pedido;
            $this->cfdi_code = $qryOrderAddress->cfdi_code;
            return true;

        } else {
            Log::debug('Error al obtener los datos de la BD ' . $qryOrderAddress);
            $this->responseError = 'No existen datos de la orden con el ID:' . $this->idOrdenShopify;
            return false;
        }

    }


    function getProductsOrder($folio)
    {
        
        $productArray = [];
        $orderTotalDiscount = 0;
        $montoGC = 0;
        $gateway = '';
        $totalOrder = 0;
        
        $products = OrderProductNc::where('folio', '=', $folio)->get();
        Log::debug('productos2 : '. json_encode($products));
        $orders = DB::table('orders')
        ->leftJoin('order_transactions', 'order_transactions.id_orden', '=', 'orders.id_orden')
        ->select(
            'orders.total_discounts as orderTotaldiscount', 
            'orders.importe_total as totalOrder', 
            'order_transactions.gateway', 
            'order_transactions.amount as montoGC'
            )
            ->where('orders.id_orden', '=', $this->idOrdenShopify)
            ->where('order_transactions.gateway', '=', 'gift_card')
            ->where('order_transactions.kind', '=', 'sale')
            ->get();
        if(!empty($orders)){
            foreach($orders as $order){
                
            }
        }

        $gift_card = $this->getGiftCard($this->idOrdenShopify);
        if(!empty($gift_card)){
            $orderTotalDiscount = $gift_card['orderTotaldiscount'];
            $montoGC = $gift_card['montoGC'];
            $gateway = $gift_card['gateway'];
            $totalOrder = $gift_card['totalOrder'];
        }

        foreach ($products as $product) {
            $productArray[] = array(
                'lineItemId' => $product->line_item_id,
                'idOrden' => $product->id_orden,
                'codigoAlfa' => $product->codigo_alfa,
                'cantidad' => $product->cantidad,
                'importe' => $product->importe,
                'referencia' => $product->referencia,
                'talla' => $product->talla,
                'title' => $product->title,
                'totalDiscount' => $product->total_discount,
                'varianTitle' => $product->variant_title,
                'sku' => $product->sku,
                'orderTotalDiscount' => $orderTotalDiscount,
                'montoGC' => $montoGC,
                'gateway' => $gateway,
                'totalOrder' => $totalOrder,
                'precio_base'=> $product->precio_base,
                'descuento_base'=> $product->descuento_base
                );
        }
        // Log::debug($productArray);
        $this->productsData = $productArray;
        return true;
    }

    public function getProductsOrderByOrderId($statusProduct)
    {
        $orderTotalDiscount = 0;
        $montoGC = 0;
        $gateway = '';
        $totalOrder = 0;
        //if( $statusProduct <> '' ){ $sqlStatement = " AND product_status='{$statusProduct}'";}
        $query = DB::table('order_products')
                // ->leftJoin('orders', 'orders.id_orden', '=', 'order_products.id_orden')
                // ->leftJoin('order_transactions', 'order_transactions.id_orden', '=', 'orders.id_orden')
                // ->select(
                    // 'order_products.*', 
                    // 'orders.total_discounts as orderTotaldiscount', 
                    // 'orders.importe_total as totalOrder', 
                    // 'order_transactions.gateway', 
                    // 'order_transactions.amount as montoGC')
                ->where('order_products.id_orden', '=', $this->idOrdenShopify);

         $gift_card = $this->getGiftCard($this->idOrdenShopify);
         if(!empty($gift_card)){
            $orderTotalDiscount = $gift_card['orderTotaldiscount'];
            $montoGC = $gift_card['montoGC'];
            $gateway = $gift_card['gateway'];
            $totalOrder = $gift_card['totalOrder'];
        }

        if ($statusProduct == 'reembolso') {
            $sqlStatement = "'devolucion', '>', 0";
            $query->where('devolucion', '>', 0);
            $qtyField = 'devolucion';
        } elseif ($statusProduct == 'cancelado') {
            $sqlStatement = "'cancelacion', '>', 0";
            $query->where('cancelacion', '>', 0);
            $qtyField = 'cancelacion';
        } elseif ($statusProduct == 'preparado') {
            $sqlStatement = "'envio', '>', 0";
            $query->where('envio', '>', 0);
            $qtyField = 'envio';
        } elseif ($statusProduct == 'nota_credito') {
            $sqlStatement = "'nota_credito', '>', 0";
            $query->where('nota_credito', '>', 0);
            $qtyField = 'nota_credito';
        } else { $sqlStatement = "";
            $qtyField = 'cantidad';
        }
        $products = $query->get();
        
        // $products = Product::where([['id_orden', '=', $this->idOrdenShopify], [$sqlStatement]])->get();
        if ($products->count() > 0) {
            foreach ($products as $product) {
                $productArray[] = array(
                    'lineItemId' => $product->line_item_id,
                    'idOrden' => $product->id_orden,
                    'codigoAlfa' => $product->codigo_alfa,
                    'cantidad' => $product->$qtyField,
                    'importe' => $product->importe,
                    'productStatus' => $product->product_status,
                    'referencia' => $product->referencia,
                    'talla' => $product->talla,
                    'title' => $product->title,
                    'totalDiscount' => $product->total_discount,
                    'varianTitle' => $product->variant_title,
                    'sku' => $product->sku,
                    'orderTotalDiscount' => $orderTotalDiscount,
                    'montoGC' => $montoGC,
                    'gateway' => $gateway,
                    'totalOrder' => $totalOrder,
                    'precio_base'=> $product->precio_base,
                    'descuento_base'=> $product->descuento_base
                );
            }
            $this->productsData = $productArray;
            return true;

        } else {

            Log::debug('No existen productos para la ID Orden: ' . $this->idOrdenShopify);
            // $this->responseError = 'No existen productos para la ID Orden:' . $this->idOrdenShopify;
            return false;
        }

    }

    public function getGiftCard($id_shopify)
    {
        $gift_card = [];
        $orders = DB::table('orders')
                ->leftJoin('order_transactions', 'order_transactions.id_orden', '=', 'orders.id_orden')
                ->select(
                    'orders.total_discounts as orderTotaldiscount', 
                    'orders.importe_total as totalOrder', 
                    'order_transactions.gateway', 
                    'order_transactions.amount as montoGC'
                    )
                    ->where('orders.id_orden', '=', $id_shopify)
                    ->where('order_transactions.gateway', '=', 'gift_card')
                    ->where('order_transactions.kind', '=', 'sale')
                    ->get();
                if(!empty($orders)){
                    foreach($orders as $order){
                        $gift_card = array(
                            'orderTotalDiscount' => $order->orderTotaldiscount,
                            'montoGC' =>  $order->montoGC,
                            'gateway' => $order->gateway,
                            'totalOrder' => $order->totalOrder
                        );
                        // $orderTotalDiscount = $order->orderTotaldiscount;
                        // $montoGC = $order->montoGC;
                        // $gateway = $order->gateway;
                        // $totalOrder = $order->totalOrder;
                    }
                }

                return $gift_card;
    }


    public function getTotalAmountByProductStatus()
    {
        // $sqlStr = '';
        // dd($this);
        if ($this->productStatus == 'reembolso') {
            $qtyField = 'devolucion';
        } elseif ($this->productStatus == 'cancelado') {
            $qtyField = 'cancelacion';
        } elseif ($this->productStatus == 'preparado') {
            $qtyField = 'envio';
        } elseif ($this->productStatus == 'nota_credito') {
            $qtyField = 'nota_credito';
        } else {
            $sqlStatement = "";
            $qtyField = 'cantidad';
        }
        // $qry = DB::table('qry_product_bill_calc')
        //     ->select(DB::raw(
        //         'SUM(linea_precio_unitario*' . $qtyField . ') AS monto_subtotal,
        //         SUM(linea_monto_iva) AS monto_impuestos,
        //         SUM(linea_precio_unitario*' . $qtyField . ')-SUM(linea_monto_descuento) AS monto_total,
        //         SUM(linea_monto_descuento) AS monto_descuento,
        //         SUM(linea_monto_descuento) AS monto_total_descuentos,
        //         (SUM(linea_precio_unitario*' . $qtyField . ')+SUM(linea_monto_iva))-SUM(linea_monto_descuento) AS monto_total_pagar, product_status'))
        //     ->where('id_orden', '=', $this->idOrdenShopify)
        //     ->where($qtyField, '>', 0)
        //     ->get();
        $qry = DB::table('qry_order_bill_calc')
            ->where('id_orden', '=', $this->idOrdenShopify)
            ->first();
        $qry = QryOrderBillCalc::where('id_orden', $this->idOrdenShopify)->first();
        if (!empty($qry)) {
            $this->orderMontoSubtotal = $qry->monto_subtotal;
            $this->orderMontoImpuestos = $qry->monto_impuestos;
            $this->orderMontoTotal = $qry->monto_total;
            $this->orderMontoDescuento = $qry->monto_descuento;
            $this->orderMontoTotalDescuentos = $qry->monto_total_descuento;
            $this->orderMontoTotalPagar = $qry->monto_total_pagar;

            return true;

        } else {
            Log::debug('GetTotalAmountByProductStatus Info: No existen productos para la orden ID ' . $this->idOrdenShopify);
            // $this->responseError = 'GetTotalAmountByProductStatus Info: No existen productos para la orden ID ' . $this->idOrdenShopify;
            return false;
        }
    }

}
