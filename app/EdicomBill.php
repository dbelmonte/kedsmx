<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class EdicomBill extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $table = "edicom_bills";
    protected $fillable = [
        'folio', 'serie', 'id_orden', 'orden_num', 'bill_status', 'rfc', 'email', 'file_contents', 'edi_contents', 'file_b64', 'verification_code',
        'bill_type', 'error_msg', 'created', 'updated', 'order_maskID', 'uuid_factura', 'uuid_factura_relacional'
    ];
    protected $primaryKey = 'folio';
    public $timestamps = false;
    protected $guarded = array('folio', 'serie');


    public function scopebySerieFa(Builder $query, $id)
    {
        $query->where('id_orden', '=', $id)->where('serie', '=', config('app.SERIE_FACTURA'));
    }
    
}
