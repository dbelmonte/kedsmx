<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QryOrderAddress extends Model
{
    protected $table = 'qry_order_addresses';
    public $timestamps = false;
    protected $primaryKey = 'id_orden';
    public $incrementing = false;
    protected $fillable = [
        'id_orden', 'tipo_orden', 'fecha_pedido', 'email_envio', 'importe_total', 'payment_status', 'id_orden_hermes', 'requiere_factura', 'cliente_rfc',
        'shipping_method', 'cac_store_id', 'order_status', 'total_discounts', 'direccion_tipo', 'nombre', 'apellido_1', 'apellido_2', 'calle', 'no_interior',
        'no_exterior', 'colonia', 'poblacion', 'provincia', 'pais', 'codigo_postal', 'telefono',
    ];
}
